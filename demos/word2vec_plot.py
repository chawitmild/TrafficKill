import emoji
import matplotlib.font_manager
import pandas as pd
import numpy as np
import numpy.random as nr
import matplotlib.pyplot as plt
import models.word2vec.utility as util
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE


nr.seed(1)

pretrain_vocab_file_path = 'trained_models/escort_vocab_mini5_128len_v2.txt'
pretrain_embedding_file_path = 'trained_models/escort_embedding_mini5_128len_v2'

pretrain_vocab = pd.read_csv(pretrain_vocab_file_path, header=None, sep=' ')
pretrain_embedding = np.loadtxt(pretrain_embedding_file_path)

# pretrain_vocab produced by current word2vec implementation will have
# 2 columns, 1st for words where their order matches the embedding, and
# 2nd for frequency counts which we don't need.
pretrain_id2word = pretrain_vocab[0].tolist()
pretrain_word2id = {}
for i, word in enumerate(pretrain_id2word):
    # Manually fix a small technical issue: pandas reads 'null' from the
    # text file as nan, which will cause problems.
    if word is np.NaN:
        word = 'null'
        pretrain_id2word[i] = 'null'
    pretrain_word2id[word] = i

# Extract the emojis from pretrain vocabulary.
full_emoji_list = emoji.UNICODE_EMOJI.keys()
emoji_id2word = []
for i in xrange(len(pretrain_id2word)):
    if pretrain_id2word[i].decode('utf-8') in full_emoji_list:
        emoji_id2word.append(pretrain_id2word[i])

emoji_word2id = {}
for i, word in enumerate(emoji_id2word):
    emoji_word2id[word] = i

# Extract the emojis' embeddings.
# Here we don't use construct_restricted_embedding() because it creates extra rows
# for 0-padding and 'UNK' which we don't need.
emoji_embedding = np.zeros(shape=(len(emoji_id2word), pretrain_embedding.shape[1]))
for word in emoji_word2id.keys():
    emoji_embedding[emoji_word2id[word]] = pretrain_embedding[pretrain_word2id[word]]

# Make sure you have a font that can display emojis, such as installing
# https://github.com/eosrei/emojione-color-font#install-on-linux
# and set the font property accordingly
font_prop = matplotlib.font_manager.FontProperties(
    fname='/usr/share/fonts/truetype/emoji/EmojiOneColor-SVGinOT.ttf')

# PCA. Optionally one can use pretrain_embedding instead of emoji_embedding when
# fitting model
pca_model = PCA(n_components=2)
pca_model.fit(emoji_embedding)
emoji_embedding_pca = pca_model.transform(emoji_embedding)

# Make plot. For visibility we only plot a subset of emojis
idx = nr.choice(range(len(emoji_id2word)), size=200, replace=False)
plt.rcParams["figure.figsize"] = [12.0, 8.0]
plt.rcParams['figure.dpi'] = 170
plt.scatter(emoji_embedding_pca[idx, 0], emoji_embedding_pca[idx, 1], alpha=0)
for i, x, y in zip(idx, emoji_embedding_pca[idx, 0], emoji_embedding_pca[idx, 1]):
    plt.annotate(emoji_id2word[i].decode('utf-8'), xy=(x, y), xytext=(5, -5),
        textcoords='offset points', ha='right', va='bottom',
        fontproperties=font_prop, fontsize=10)
plt.show()

# Go through similar steps with TSNE instead of PCA. Note that to get the same result
# as the saved picture, need to run all randomness-related code above, namely
# nr.choice().
tsne_model = TSNE(n_components=2)
emoji_embedding_tsne = tsne_model.fit_transform(emoji_embedding)

idx = range(200)
plt.scatter(emoji_embedding_tsne[idx, 0], emoji_embedding_tsne[idx, 1], alpha=0)
for i, x, y in zip(idx, emoji_embedding_tsne[idx, 0], emoji_embedding_tsne[idx, 1]):
    plt.annotate(emoji_id2word[i].decode('utf-8'), xy=(x, y), xytext=(5, -5),
        textcoords='offset points', ha='right', va='bottom',
        fontproperties=font_prop, fontsize=10)
plt.xlim(-12, 3.5)
plt.ylim(-8, 8)
plt.savefig("demos/emoji_embedding_tSNE.svg", format="svg")
plt.show()
# After toying with the zooming, the above x and y limits yield a pretty good view.
# Notice how emojis with similar meanings cluster. E.g., hearts, smileys, phones,
# flowers, etc. More interestingly, we can identify the obscure emojis with sensitive
# meanings. E.g., the lit bomb is close to lips, tongue, and saliva which all
# indicate blow job. Strawberry and grapes are close to cherry; The ice creams and
# candy are close to lollipop. Cherry and lollipop are known indicators of under-aged
# providers, and now we know their alternatives.

# Also tried Multidimensional scaling but the separation was not nearly as good.

# Output emoji vocab for easy viewing
pd.DataFrame(data={'emoji': emoji_id2word, 'x_coord': emoji_embedding_tsne[:, 0],
                   'y_coord': emoji_embedding_tsne[:, 1]}).to_csv('temp/emoji.csv')

# Add a wrapper to print out similar words for the given one
def match_word(word, source='emoji', k=5):
    if source == 'emoji':
        matched_words, matched_ids, similarities = util.find_top_match_word(
            word, emoji_word2id, emoji_id2word, emoji_embedding, k, True)
        for i in xrange(k):
            print matched_words[i], matched_ids[i], similarities[i]
    else:
        matched_words, matched_ids, similarities = util.find_top_match_word(
            word, pretrain_word2id, pretrain_id2word, pretrain_embedding, k, True)
        for i in xrange(k):
            print matched_words[i], matched_ids[i], similarities[i]

    return

match_word(emoji_id2word[1], source='pretrain', k=10)