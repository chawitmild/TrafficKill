import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity


# Find top k vectors in the embedding matrix that match the given word vector
def find_top_match_vec(vec, emb, k=5):
    similarities = cosine_similarity(vec.reshape(1, -1), emb).reshape(-1)
    matched_ids = np.argpartition(similarities, -k)[-k:]
    matched_ids = matched_ids[np.argsort(-similarities[matched_ids])]

    return matched_ids, similarities[matched_ids], emb[matched_ids]


# Find top k words that match the given word
def find_top_match_word(word, word2id, id2word, emb, k=5, exclude_self=True):
    if word not in word2id:
        print word + " is not in vocabulary"
        return
    if exclude_self:
        k += 1

    matched_ids, similarities, _ = find_top_match_vec(vec=emb[word2id[word]],
                                                      emb=emb, k=k)
    matched_words = [id2word[i] for i in matched_ids]

    if not exclude_self:
        return matched_words, matched_ids, similarities
    else:
        return matched_words[1:], matched_ids[1:], similarities[1:]


# Find top k analogies for the given words, i.e., top choices for w3 in w1-w0 vs
# w3-w2.
def find_top_match_analogy(w0, w1, w2, word2id, id2word, emb, k=5,
                           exclude_self=True):
    for w in [w0, w1, w2]:
        if w not in word2id:
            print w + ' is not in vocabulary'
            return
    if exclude_self:
        k += 3

    # Note that equivalently, we can find w3 vs w1-w0+w2.
    vec = emb[word2id[w1]] - emb[word2id[w0]] + emb[word2id[w2]]
    matched_ids, similarities, _ = find_top_match_vec(vec=vec, emb=emb, k=k)
    matched_words = [id2word[i] for i in matched_ids]

    if exclude_self:
        # Exclude words from the input
        matched_ids = matched_ids[[w not in [w0, w1, w2] for w in matched_words]][:k-3]
        similarities = similarities[[w not in [w0, w1, w2] for w in matched_words]][:k-3]
        matched_words = [id2word[i] for i in matched_ids][:k-3]

    return matched_words, matched_ids, similarities


if __name__ == '__main__':
    # Load vocabulary
    # vocab = pd.read_csv('trained_models/text8_vocab.txt', header=None, sep=' ')
    vocab = pd.read_csv('trained_models/escort_vocab.txt', header=None, sep=' ')
    id2word = vocab[0].tolist()
    # Manually fix a small technical issue: pandas reads 'null' from the text file as
    # nan, which will cause problems.
    for i, w in enumerate(id2word):
        if w is np.NaN:
            id2word[i] = 'null'
    word2id = {}
    for i, w in enumerate(id2word):
        word2id[w] = i

    # Load embedding
    # emb = np.loadtxt('trained_models/text8_final_embedding')
    emb = np.loadtxt('trained_models/escort_final_embedding')

    # Testing
    find_top_match_word('he', word2id=word2id, id2word=id2word, emb=emb, k=5,
                        exclude_self=False)
    find_top_match_word('he', word2id=word2id, id2word=id2word, emb=emb, k=5,
                        exclude_self=True)
    find_top_match_analogy('big', 'bigger', 'long', word2id, id2word, emb, k=5,
                           exclude_self=False)
    find_top_match_analogy('big', 'bigger', 'long', word2id, id2word, emb, k=5,
                           exclude_self=True)