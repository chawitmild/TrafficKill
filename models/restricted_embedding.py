"""
Restrict the (full) embedding trained by word2vec model to the given vocabulary,
which typically comes from the training set.
"""
import os
import copy
import numpy as np
import numpy.random as nr
import cPickle as pickle


def construct_restricted_embedding(
        restricted_word2id, pretrain_word2id, pretrain_embedding,
        restricted_word2id_encoding=None, pretrain_word2id_encoding='utf-8'):
    """
    Construct restricted embedding by restricting pretrain_embedding to the
    vocabulary of restricted_word2id.
    :param restricted_word2id: Word to index dictionary for the restricted
    embedding. Note that index 0 should be reserved for empty string/0-padding, which
    is the case for word2id produced by CountVectorizer() from sklearn.
    :param pretrain_word2id: Word to index dictionary for the (full) embedding
    pretrained by word2vec model.
    :param pretrain_embedding: Embedding pretrained by word2vec model. A numpy
    array. The i-th row is the embedding vector for word with index i in
    pretrain_word2id.
    :param restricted_word2id_encoding: Encoding for restricted_word2id. Currently,
    word2id constructed by GFRNN() will have None.
    :param pretrain_word2id_encoding: Encoding for pretrain_word2id. Note that the
    word2id produced by the current word2vec model will have utf-8 encoding.
    :return: Restricted embedding where the i-th row is the embedding vector for
    word with index i in restricted_word2id.
    """
    # restricted_word2id and pretrain_word2id need to have the same encoding for the
    # same words to be properly recognized by both. Here we make sure that they are
    # both in unicode.
    if restricted_word2id_encoding is not None:
        # Copy the dictionary before making changes since dictionary is mutable.
        restricted_word2id = copy.copy(restricted_word2id)
        for word in restricted_word2id.keys():
            restricted_word2id[word.decode(restricted_word2id_encoding)] = \
                restricted_word2id.pop(word)
    if pretrain_word2id_encoding is not None:
        pretrain_word2id = copy.copy(pretrain_word2id)
        for word in pretrain_word2id.keys():
            pretrain_word2id[word.decode(pretrain_word2id_encoding)] = \
                pretrain_word2id.pop(word)

    # Start with a uniformly sampled embedding. If a word from restricted_word2id is
    # found in pretrain_word2id, fill the corresponding row with pretrained embedding
    # vector.
    # Special cases: Index 0 is for empty string/0-padding, so 1st row of embedding
    # matrix will all be 0's. Last row is for all unknown words. The pretrain_word2id
    # actually has a token 'UNK' so we can fill that row with its vector. But it can
    # also be left random.

    # 0 should already be in restricted_word2id so we just need +1 for unknown.
    token_num = len(restricted_word2id) + 1
    embedding_vector_length = pretrain_embedding.shape[1]
    restricted_embedding = nr.uniform(low=-0.1, high=0.1,
                                      size=(token_num, embedding_vector_length))

    num_words_outside_pretrain = 0
    for word in restricted_word2id.keys():
        if word in pretrain_word2id.keys():
            restricted_embedding[restricted_word2id[word]] = \
                pretrain_embedding[pretrain_word2id[word]]
        else:
            num_words_outside_pretrain += 1
    print "Number of words without pretrained embedding: %4d, percentage: %0.3f" % (
        num_words_outside_pretrain,
        num_words_outside_pretrain / float(len(restricted_word2id)))

    # Handle the special cases
    restricted_embedding[0] = 0
    try:
        restricted_embedding[-1] = pretrain_embedding[pretrain_word2id['UNK']]
    except KeyError:
        pass

    return restricted_embedding