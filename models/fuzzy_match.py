import os
import sys
import math
import time
import copy
import numpy as np
import numpy.random as nr
import pandas as pd
import cPickle as pickle
from fuzzywuzzy import fuzz, process


class FuzzyMatcher:
    def __init__(self, data, text_var_list, drop_original_text_var=True):
        """
        Initializes the datasets
        :param data: Raw data. Must be a pandas dataframe.
        :param text_var_list: List of variable that contain texts to be used during
        search.
        :param drop_original_text_var: Whether to drop the variables in text_var_list
        after combining them into a new column.
        """
        self.data = copy.deepcopy(data)
        self.text_var_list = text_var_list
        # Join the texts from all given variables
        self.data['text_all'] = data[text_var_list[0]]
        for i in xrange(1, len(text_var_list)):
            self.data['text_all'] = self.data['text_all'] + ' ' + \
                                    data[text_var_list[i]]
        if drop_original_text_var:
            self.data.drop(text_var_list, axis=1, inplace=True)

        self.input_string = None
        self.match_result = None

    def match(self, input_string, scorer=fuzz.token_set_ratio, limit=5,
              full_info=True, sort_var_list=['score', 'text_all'], verbose=True):
        """
        Fuzzy match the input_string with entries from self.data['text_all'].
        :param input_string: String of key words or sentences.
        :param scorer: Scorer used in fuzzy match algorithm. For data with emoji
        valid choices are [fuzz.token_set_ratio, fuzz.partial_token_set_ratio,
        fuzz.WRatio]. See https://github.com/seatgeek/fuzzywuzzy for more details.
        :param limit: Limit of returned result (which will be sorted based on score).
        :param full_info: Whether the other columns in the data should be included
        in the returned result. If True, a subset of the original dataframe with the
        matched entries will be created and ready for further manipulation such as
        drop_duplicates() and save_match_result(), otherwise only a list of (text,
        score) tuples will be returned.
        :param sort_var_list: List of variables used in sorting the result.
        :param verbose: Whether to print misc info such as run time.
        :return: Match result, whose format depends on full_info.
        """
        self.input_string = input_string
        start_time = time.time()
        result = process.extract(input_string, self.data['text_all'], limit=limit,
                                 scorer=scorer)
        if verbose:
            print 'Minutes to complete fuzzy match: %5.3f' % (
                (time.time() - start_time) / 60)

        if not full_info:
            # If full_info is not required, end here.
            return result

        # The output of extract() is a list of tuples. We separate it into 2 lists.
        result_text, result_score = map(list, zip(*result))
        # Subset the original dataframe with entries in result_text
        self.match_result = self.data[self.data['text_all'].isin(result_text)]
        # Add a column of scores in the new dataframe
        for i in xrange(len(result_text)):
            self.match_result.loc[self.match_result['text_all'] == result_text[i],
                                  'score'] = result_score[i]
        # Sort the new dataframe based on score. Note: using inplace=True in
        # sort_values() produces an annoying warning.
        self.match_result = self.match_result.sort_values(by=sort_var_list,
                                                          ascending=False)

        return self.match_result

    def drop_duplicates(self, all_var=False, use_text_all=False, use_score=False,
                        var_list=[], in_place=False):
        """
        Drop duplicates in the match_result using variables from var_list.
        E.g., To see the same text posted under different user IDs, one could set
        use_text_var=True and var_list=['user_id']. To see the same user posting from
        different locations, one could set var_list=['user_id', 'location']. To
        remove texts with slight differences but same scores, one could set
        use_score=True in addition to the desired var_list.
        :param all_var: Whether to use all the variables. If True, overrides the
        other arguments except for in_place.
        :param use_text_all: Whether to add ['text_all'] to var_list.
        :param use_score: Whether to add ['score'] to var_list.
        :param var_list: List of variables used when dropping duplicates.
        :param in_place: Whether to modify self.match_result or return a new dataset.
        :return: Deduplicated match result.
        """
        if self.match_result is None:
            raise Exception('Must run match() first')

        if all_var:
            if in_place:
                self.match_result.drop_duplicates(inplace=True)
                return
            else:
                return self.match_result.drop_duplicates(inplace=False)

        if use_text_all:
            var_list += ['text_all']
        if use_score:
            var_list += ['score']

        if in_place:
            self.match_result.drop_duplicates(subset=var_list, inplace=True)
        else:
            return self.match_result.drop_duplicates(subset=var_list, inplace=False)

    def further_match(self, input_string, scorer=fuzz.WRatio, limit=5,
                      full_info=True, sort_var_list=['score', 'text_all'],
                      verbose=True, in_place=False):
        """
        Search from the previous match result. Useful when the scorer desired is slow
        on large dataset, and a pre-screening with a fast scorer is necessary. E.g.,
        Use fuzz.token_set_ratio to pre-screen and then use fuzz.WRatio for further
        matching.
        """
        start_time = time.time()
        result = process.extract(input_string, self.match_result['text_all'],
                                 limit=limit, scorer=scorer)
        if verbose:
            print 'Minutes to complete further match: %5.3f' % (
                (time.time() - start_time) / 60)

        if not full_info:
            # If full_info is not required, end here.
            return result

        # The output of extract() is a list of tuples. We separate it into 2 lists.
        result_text, result_score = map(list, zip(*result))
        # Subset the previous match result with entries in the new result_text
        further_match_result = self.data[self.data['text_all'].isin(result_text)]
        # Add a column of scores in the new dataframe
        for i in xrange(len(result_text)):
            further_match_result.loc[self.match_result['text_all'] == result_text[i],
                                     'score'] = result_score[i]
        # Sort the new dataframe based on score. Note: using inplace=True in
        # sort_values() produces an annoying warning.
        further_match_result = further_match_result.sort_values(by=sort_var_list,
                                                                ascending=False)
        if in_place:
            self.input_string = input_string
            self.match_result = further_match_result

        return further_match_result

    def save_match_result(self, result_file_path):
        # Add input string to the data for record keeping
        self.match_result.loc[:, 'input_string'] = self.input_string

        self.match_result.to_csv(result_file_path)

