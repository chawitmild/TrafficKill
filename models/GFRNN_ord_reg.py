"""
This file implements several variations of the ordinal regression layer, including
one based on https://goo.gl/T5jX6z, and my own designs.
"""
import os
import abc
import sys
import math
import time
import copy
import torch
import utility
import numpy as np
import numpy.random as nr
import pandas as pd
import cPickle as pickle
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.nn.modules.rnn import RNNCellBase
from torch.nn.parameter import Parameter
from torch.utils.data import DataLoader, TensorDataset
from GFRNN import GFRUNetwork
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import confusion_matrix
from restricted_embedding import construct_restricted_embedding


class SeparateBinaryOrdReg(nn.Module):
    def __init__(self, input_size, num_target_classes):
        super(SeparateBinaryOrdReg, self).__init__()
        self.input_size = input_size
        self.num_target_classes = num_target_classes

        # Only need num_target_classes - 1 classifiers, as the i-th classifier models
        # whether the label is strictly greater than i, where i starts from 0.
        binary_classifier_list = [None] * (num_target_classes - 1)
        for i in xrange(num_target_classes - 1):
            binary_classifier_list[i] = nn.Linear(input_size, 2)
        self.binary_classifier_list = nn.ModuleList(binary_classifier_list)
        self.softmax = nn.Softmax()

        self.initialize_parameters()

    def initialize_parameters(self):
        for classifier in self.binary_classifier_list:
            classifier.bias.data.fill_(0)
            classifier.weight.data.uniform_(-0.1, 0.1)

    def forward(self, input_tensor, output_prob=False):
        prediction = []
        for classifier in self.binary_classifier_list:
            # During training, we don't apply softmax to the output, because the loss
            # function nn.CrossEntropyLoss() computes log softmax for us.
            if not output_prob:
                prediction.append(classifier(input_tensor))
            # Apply softmax if we want the output to be probabilities. We can also
            # apply utility.softmax after converting the ouput to numpy arrays
            # outside of this function.
            else:
                prediction.append(self.softmax(classifier(input_tensor)))
        # Output prediction is of shape (n_classifier, n_sample, 2), so prediction[i]
        # contains the predictions from the i-th classifier.
        prediction = torch.cat(prediction, 0).view(self.num_target_classes - 1,
                                                   input_tensor.size(0), 2)

        return prediction


class JointSigmoidOrdReg(nn.Module):
    def __init__(self, input_size, num_target_classes):
        super(JointSigmoidOrdReg, self).__init__()
        self.input_size = input_size
        self.num_target_classes = num_target_classes

        self.joint_classifier = nn.Sequential(
            nn.Linear(input_size, num_target_classes - 1),
            nn.Sigmoid()
        )

        self.initialize_parameters()

    def initialize_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Linear):
                m.bias.data.fill_(0)
                m.weight.data.uniform_(-0.1, 0.1)

    def forward(self, input_tensor):

        return self.joint_classifier(input_tensor)


class OrdinalRNNModel(nn.Module):
    def __init__(self, token_num, embedding_vector_length=128, hidden_size=128,
                 rnn_layer_num=3, rnn_type='GFRU', num_target_classes=3, dropout=0.5,
                 batch_normalization=True, output_layer_type='last',
                 residual_connection=True, ordinal_reg_type='separate'):
        super(OrdinalRNNModel, self).__init__()
        self.drop = nn.Dropout(dropout)
        self.token_num = token_num
        self.encoder = nn.Embedding(self.token_num, embedding_vector_length)
        if rnn_type == 'GFRU':
            self.rnn = GFRUNetwork(embedding_vector_length, hidden_size, rnn_layer_num, dropout=dropout,
                                   residual_connection=residual_connection)
        elif rnn_type == 'LSTM':
            self.rnn = nn.LSTM(embedding_vector_length, hidden_size, rnn_layer_num, dropout=dropout)
        else:
            raise ValueError("Invalid rnn type. Choices are 'GFRU' and 'LSTM'.")

        self.num_target_classes = num_target_classes
        self.embedding_vector_length = embedding_vector_length
        self.hidden_size = hidden_size
        self.rnn_layer_num = rnn_layer_num
        self.batch_normalization = batch_normalization
        self.batch_norm_embedding_vec = nn.BatchNorm1d(embedding_vector_length)
        self.batch_norm_rnn_output = nn.BatchNorm1d(hidden_size)
        self.output_layer_type = output_layer_type
        if output_layer_type not in ['last', 'mean-pool']:
            raise ValueError("Invalid output layer type. Choices are 'last' and 'mean-pool'.")

        self.ordinal_reg_type = ordinal_reg_type
        if ordinal_reg_type == 'separate':
            self.ordinal_reg_layer = SeparateBinaryOrdReg(hidden_size, num_target_classes)
        elif ordinal_reg_type == 'joint':
            self.ordinal_reg_layer = JointSigmoidOrdReg(hidden_size, num_target_classes)
        else:
            raise ValueError("Invalid ordinal regression type. Choices are 'separate' and 'joint'.")

        self.initialize_parameters()

    def initialize_parameters(self):
        self.encoder.weight.data.uniform_(-0.1, 0.1)

    def forward(self, input_tensor, prev_states, batch_first=True):
        if batch_first:
            input_tensor = input_tensor.t().contiguous()

        embedding = self.drop(self.encoder(input_tensor))

        if self.batch_normalization:
            embedding = self.batch_norm_embedding_vec(
                embedding.permute(1, 2, 0).contiguous()).permute(2, 0, 1)

        output_tensor, final_states = self.rnn(embedding, prev_states)

        if self.output_layer_type == 'last':
            output_tensor = self.drop(output_tensor[-1])

        elif self.output_layer_type == 'mean-pool':
            output_tensor = self.drop(torch.squeeze(torch.mean(output_tensor, dim=0)))

        if self.batch_normalization:
            output_tensor = self.batch_norm_rnn_output(output_tensor)

        prediction = self.ordinal_reg_layer(output_tensor)

        return prediction

    def initialize_states(self, batch_size):
        weight = next(self.parameters()).data

        return Variable(weight.new(self.rnn_layer_num, batch_size, self.hidden_size).zero_()), \
               Variable(weight.new(self.rnn_layer_num, batch_size, self.hidden_size).zero_())


def OrdinalRNNModelEval(model, x_val, y_val, num_target_classes,
                        formatted_y_val=None, max_eval_batch_size=10000,
                        use_gpu=False, tolerance=0, task_weight=None,
                        order_penalty=0, ordinal_reg_type='separate'):
    model.eval()
    if use_gpu:
        model.cuda()
    else:
        model.cpu()

    if formatted_y_val is None:
        formatted_y_val = torch.LongTensor(utility.OrdinalLabelReformat(
            label=y_val, num_target_classes=num_target_classes))

    xy_val = TensorDataset(torch.LongTensor(x_val), torch.LongTensor(y_val))
    eval_loader = DataLoader(xy_val, max_eval_batch_size, shuffle=False, drop_last=False)
    for i, xy_val_batch in enumerate(eval_loader, 0):

        x_val_batch, _ = xy_val_batch
        if use_gpu and not x_val_batch.is_cuda:
            x_val_batch = x_val_batch.cuda()
        current_eval_batch_size = x_val_batch.size(0)
        initial_states = model.initialize_states(current_eval_batch_size)
        # Note that for 'separate', the output is only predicted probabilities after
        # applying softmax to the last dimension. But we'll still use pred_prob to
        # distinguish from pred_label.
        batch_pred_prob = model(Variable(x_val_batch), initial_states, True).data
        if i == 0:
            pred_prob = batch_pred_prob
        else:
            # For 'separate' ordinal reg type, sample size is in dimension 1, for
            # 'joint', sample size is in dimension 0.
            if ordinal_reg_type == 'separate':
                pred_prob = torch.cat([pred_prob, batch_pred_prob], dim=1)
            else:
                pred_prob = torch.cat([pred_prob, batch_pred_prob], dim=0)

    ord_reg_loss = utility.OrdRegLoss(
        prediction=Variable(pred_prob), label=formatted_y_val,
        num_target_classes=num_target_classes, use_gpu=use_gpu,
        task_weight=task_weight, ordinal_reg_type=ordinal_reg_type,
        order_penalty=order_penalty).data.cpu().numpy()[0]

    # Convert predicted probabilities to predicted labels for the other two metrics
    pred_prob = pred_prob.cpu().numpy()
    if ordinal_reg_type == 'separate':
        pred_prob = utility.Softmax(pred_prob)
    pred_label = utility.Prob2Label(prediction=pred_prob,
                                    ordinal_reg_type=ordinal_reg_type)

    mean_abs_error = utility.MeanAbsError(prediction=pred_label, label=y_val)

    cumulative_score = utility.CumulativeScore(prediction=pred_label, label=y_val,
                                               tolerance=tolerance)

    return ord_reg_loss, mean_abs_error, cumulative_score, pred_prob, pred_label


def OrdinalRNNModelTrain(model, x_train, y_train, lr=3.0, task_weight=None,
                         order_penalty = 0, ordinal_reg_type = 'separate',
                         batch_size=200, l2_penalty=0, grad_clip=0.25, use_gpu=False,
                         embedding_lr_scale=1, steps_per_epoch=1000, verbose=True):
    """
    Function for 1 epoch of model training.
    :param model: Pytorch model to be trained.
    :param x_train: Features. Pytorch tensor of shape (n_sample, n_feature).
    :param y_train: Formatted labels produced by OrdinalLabelReformat(). Pytorch
    tensor of shape (n_sample, n_class - 1).
    """
    model.train()
    if use_gpu:
        model.cuda()
        if (not x_train.is_cuda) or (not y_train.is_cuda):
            x_train = x_train.cuda()
            y_train = y_train.cuda()
    else:
        model.cpu()
        if x_train.is_cuda or y_train.is_cuda:
            x_train = x_train.cpu()
            y_train = y_train.cpu()

    if embedding_lr_scale == 1:
        optimizer = optim.SGD(model.parameters(), lr=lr, weight_decay=l2_penalty)
    else:
        embedding_params_list = list(map(id, model.encoder.parameters()))
        non_embedding_params = filter(lambda p: id(p) not in embedding_params_list, model.parameters())
        optimizer = optim.SGD([{'params': non_embedding_params},
                               {'params': model.encoder.parameters(), 'lr': lr * embedding_lr_scale}],
                              lr=lr, weight_decay=l2_penalty)

    num_target_classes = y_train.size(1) + 1
    num_sample = x_train.size(0)
    avg_batch_loss = 0

    for step in xrange(steps_per_epoch):

        # Draw a random batch
        if use_gpu:
            batch_idx = torch.cuda.LongTensor(nr.randint(low=0, high=num_sample,
                                                         size=batch_size))
        else:
            batch_idx = torch.LongTensor(nr.randint(low=0, high=num_sample,
                                                    size=batch_size))
        x_train_batch = x_train[batch_idx]
        y_train_batch = y_train[batch_idx]

        initial_states = model.initialize_states(batch_size)

        optimizer.zero_grad()
        batch_pred_prob = model(Variable(x_train_batch), initial_states, True)
        loss = utility.OrdRegLoss(prediction=batch_pred_prob, label=y_train_batch,
                                  num_target_classes=num_target_classes,
                                  use_gpu=use_gpu, task_weight=task_weight,
                                  ordinal_reg_type=ordinal_reg_type,
                                  order_penalty=order_penalty)
        loss.backward()

        torch.nn.utils.clip_grad_norm(model.parameters(), grad_clip)

        optimizer.step()
        if use_gpu:
            avg_batch_loss += loss.data.cpu().numpy()[0]
        else:
            avg_batch_loss += loss.data.numpy()[0]

    avg_batch_loss /= steps_per_epoch

    return model, avg_batch_loss


class OrdinalRNNClassifier:
    def __init__(self, num_target_classes=7, embedding_vector_length=128,
                 rnn_type='GFRU', dropout=0.2, init_lr=3.0, grad_clip=0.25,
                 l2_penalty=0.00001, max_eval_batch_size=1500, max_epoch=50,
                 decay_factor=2.0, lr_decay_patience=3, early_stop_patience=9,
                 batch_size=200, seed=0, verbose=False, use_gpu=True,
                 output_layer_type='mean-pool', model_name='gfrnn',
                 hidden_size=128, rnn_layer_num=3, batch_normalization=True,
                 residual_connection=True, input_length=120, embedding_lr_scale=1.0,
                 min_frequency=2, ordinal_reg_type='joint',
                 task_weight_scheme='data-specific', cumulative_score_tolerance=0,
                 order_penalty=0.5, equalize_class_size=False,
                 pretrain_vocab_file_path=None, pretrain_embedding_file_path=None):
        self.hyper_param = \
            {'embedding_vector_length': embedding_vector_length, 'hidden_size':
             hidden_size, 'rnn_layer_num': rnn_layer_num, 'rnn_type': rnn_type,
             'dropout': dropout, 'init_lr': init_lr, 'grad_clip': grad_clip,
             'l2_penalty': l2_penalty, 'max_eval_batch_size': max_eval_batch_size,
             'max_epoch': max_epoch, 'decay_factor': decay_factor,
             'lr_decay_patience': lr_decay_patience, 'early_stop_patience':
             early_stop_patience, 'batch_size': batch_size, 'seed': seed, 'verbose':
             verbose, 'use_gpu': use_gpu, 'output_layer_type': output_layer_type,
             'model_name': model_name, 'num_target_classes': num_target_classes,
             'batch_normalization': batch_normalization, 'residual_connection':
             residual_connection, 'input_length': input_length, 'embedding_lr_scale':
             embedding_lr_scale, 'min_frequency': min_frequency, 'ordinal_reg_type':
             ordinal_reg_type, 'task_weight_scheme': task_weight_scheme,
             'cumulative_score_tolerance': cumulative_score_tolerance,
             'order_penalty': order_penalty, 'equalize_class_size': equalize_class_size,
             'pretrain_vocab_file_path': pretrain_vocab_file_path,
             'pretrain_embedding_file_path': pretrain_embedding_file_path}

        # If config.min_frequency >= 1, it represents the minimum number of docs a
        # word needs to be in, otherwise it represents the percentage.
        if min_frequency >= 1:
            self.hyper_param['min_frequency'] = int(min_frequency)

        self.word2id = None
        self.task_weight = None

        nr.seed(self.hyper_param['seed'])
        torch.manual_seed(self.hyper_param['seed'])
        if self.hyper_param['use_gpu']:
            torch.cuda.manual_seed(self.hyper_param['seed'])

    def initialize_model(self):
        # Note that this step needs to be done after build_vocabulary(), since we need
        # token_num when specifying the embedding layer (and hence the model).
        self.model = \
            OrdinalRNNModel(**dict((key, self.hyper_param[key]) for key in
                                   ('token_num', 'embedding_vector_length', 'hidden_size',
                                    'rnn_layer_num', 'rnn_type', 'num_target_classes',
                                    'dropout', 'batch_normalization', 'output_layer_type',
                                    'residual_connection', 'ordinal_reg_type')
                                   if key in self.hyper_param))

        # Load pretrained embedding if file paths were provided
        if self.hyper_param['pretrain_vocab_file_path'] is not None:
            # Load vocabulary used in pretraining
            pretrain_vocab = pd.read_csv(self.hyper_param['pretrain_vocab_file_path'],
                                         header=None, sep=' ')
            # pretrain_vocab produced by current word2vec implementation will have
            # 2 columns, 1st for words where their order matches the embedding, and
            # 2nd for frequency counts which we don't need.
            pretrain_id2word = pretrain_vocab[0].tolist()
            pretrain_word2id = {}
            for i, word in enumerate(pretrain_id2word):
                # Manually fix a small technical issue: pandas reads 'null' from the
                # text file as nan, which will cause problems.
                if word is np.NaN:
                    word = 'null'
                pretrain_word2id[word] = i
            # Load pretrained embedding
            pretrain_embedding = np.loadtxt(self.hyper_param['pretrain_embedding_file_path'])
            # Restrict the pretrained embedding to the word2id from the current training
            # set. Remember to adjust the encoding here if the codes for constructing the
            # word2id's change.
            restricted_embedding = construct_restricted_embedding(
                restricted_word2id=self.word2id, pretrain_word2id=pretrain_word2id,
                pretrain_embedding=pretrain_embedding, restricted_word2id_encoding=None,
                pretrain_word2id_encoding='utf-8')
            # Replace the randomly initialized embedding in the model with the constructed
            # embedding.
            self.model.encoder.weight.data = torch.Tensor(restricted_embedding)
            # Override the corresponding hyper parameter.
            self.hyper_param['embedding_vector_length'] = pretrain_embedding.shape[1]
            print 'Successfully imported pretrained embedding'

        print self.hyper_param

    def build_vocabulary(self, features, encoding='utf-8'):
        # We don't need the vectorizer, only the word2id that goes with it.
        vectorizer = CountVectorizer(min_df=self.hyper_param['min_frequency'],
                                     tokenizer=lambda doc: doc.lower().split(" "),
                                     encoding=encoding)
        vectorizer.fit(features)
        self.word2id = vectorizer.vocabulary_
        # +1 for the token for unknown. Refer to sentences_to_sequences().
        self.hyper_param['token_num'] = len(self.word2id) + 1

    def sentences_to_sequences(self, features, input_length=None, encoding='utf-8'):
        # Default input_length comes from self.hyper_param
        if input_length is None:
            input_length = self.hyper_param['input_length']

        id_matrix = np.zeros(shape=(features.shape[0], input_length),
                             dtype=int)
        unknown_word_count = 0
        total_word_count = 0
        for sent_pos, sentence in enumerate(features):
            # Truncate long sentences at max_length
            words = sentence.split()[:input_length]
            id_vec = np.zeros(len(words), dtype=int)
            total_word_count += len(words)
            for word_pos, word in enumerate(words):
                # Decode to unicode if the input has an encoding
                if encoding is not None:
                    word = word.decode(encoding)
                if word not in self.word2id:
                    # Map all unknown words to a single id. Note that len(word2id)
                    # is not in word2id.values().
                    id_vec[word_pos] = len(self.word2id)
                    unknown_word_count += 1
                else:
                    id_vec[word_pos] = self.word2id.get(word)
            # Note that the following line ensures that shorter sentence will be 0-padded
            # in the beginning.
            id_matrix[sent_pos, input_length - len(words):] = id_vec

        if self.hyper_param['verbose']:
            print "Unknown word ratio: {:5.3f}".format(float(unknown_word_count) / total_word_count)

        return id_matrix

    def train_model(self, x_train, y_train, x_val, y_val, encoding='utf-8'):
        """
        Model training. It's important that x_train and x_val are pandas series
        containing texts, while y_train and y_val are numpy arrays.
        """
        # Equalize class sizes if requested. Refer to EqualizeClassSize().
        if self.hyper_param['equalize_class_size']:
            x_train, y_train = utility.EqualizeClassSize(
                feature=x_train, label=y_train,
                num_target_classes=self.hyper_param['num_target_classes'],
                class_size=x_train.shape[0])

        # Compute task weight. Refer to TaskWeight().
        self.task_weight = utility.TaskWeight(
            y_train, self.hyper_param['num_target_classes'],
            self.hyper_param['task_weight_scheme'])

        # Wrap data in PyTorch tensors. Note that all the input are expected to be
        # numpy arrays. So if y_train is a pandas series, use y_train.values.
        x_train = torch.LongTensor(self.sentences_to_sequences(x_train, encoding=encoding))
        x_val = torch.LongTensor(self.sentences_to_sequences(x_val, encoding=encoding))
        formatted_y_train = torch.LongTensor(
            utility.OrdinalLabelReformat(y_train, self.hyper_param['num_target_classes']))
        formatted_y_val = torch.LongTensor(
            utility.OrdinalLabelReformat(y_val, self.hyper_param['num_target_classes']))

        lr = self.hyper_param['init_lr']
        steps_per_epoch = int(np.ceil(x_train.size(0) / float(self.hyper_param['batch_size'])))
        # Adjust steps_per_epoch accordingly if dataset has been expanded by
        # EqualizeClassSize().
        if self.hyper_param['equalize_class_size']:
            steps_per_epoch = int(np.ceil(
                steps_per_epoch / float(self.hyper_param['num_target_classes'])))

        # Perform training until reaching max_epoch, early stopping, or user interruption
        start_time = time.time()
        best_val_loss = None
        model = copy.deepcopy(self.model)
        try:
            for epoch in range(self.hyper_param['max_epoch']):
                model, train_loss = OrdinalRNNModelTrain(
                    model=model, x_train=x_train, y_train=formatted_y_train, lr=lr,
                    task_weight=self.task_weight,
                    order_penalty=self.hyper_param['order_penalty'],
                    ordinal_reg_type=self.hyper_param['ordinal_reg_type'],
                    batch_size=self.hyper_param['batch_size'],
                    l2_penalty=self.hyper_param['l2_penalty'],
                    grad_clip=self.hyper_param['grad_clip'],
                    use_gpu=self.hyper_param['use_gpu'],
                    embedding_lr_scale=self.hyper_param['embedding_lr_scale'],
                    steps_per_epoch=steps_per_epoch)

                val_loss, val_mae, val_cs, _, _ = OrdinalRNNModelEval(
                    model=model, x_val=x_val, y_val=y_val,
                    num_target_classes=self.hyper_param['num_target_classes'],
                    formatted_y_val=formatted_y_val,
                    max_eval_batch_size=self.hyper_param['max_eval_batch_size'],
                    use_gpu=self.hyper_param['use_gpu'],
                    tolerance=self.hyper_param['cumulative_score_tolerance'],
                    task_weight=self.task_weight,
                    order_penalty=self.hyper_param['order_penalty'],
                    ordinal_reg_type=self.hyper_param['ordinal_reg_type'])

                if self.hyper_param['verbose']:
                    print('| end of epoch: {:3d} | time passed: {:5.2f} min | training loss: {:5.2f} | '
                          'validation loss: {:5.2f} | validation mean abs error: {:5.3f} | '
                          'validation cumulative score: {:5.3f} | learning rate: {:5.5f}'.format(
                        epoch, (time.time() - start_time) / 60, train_loss, val_loss, val_mae,
                        val_cs, lr))

                # If best validation accuracy is achieved in the current epoch, checkpoint the model and reset learning
                # rate decay and early stopping counter.
                if not best_val_loss or val_loss < best_val_loss:
                    best_val_loss = val_loss
                    self.model = copy.deepcopy(model)
                    lr_decay_counter = 0
                    early_stop_counter = 0
                else:
                    lr_decay_counter += 1
                    early_stop_counter += 1

                # Implement learning rate decay
                # This schema decays learning rate based on validation accuracy
                if lr_decay_counter == self.hyper_param['lr_decay_patience']:
                    lr /= self.hyper_param['decay_factor']
                    lr_decay_counter = 0

                # Invoke early termination if training plateaus
                if early_stop_counter == self.hyper_param['early_stop_patience']:
                    break

        # Also allow user to manually terminate
        except KeyboardInterrupt:
            print 'Exiting from training early'

        print 'Training completed'

    def test_model(self, x_test, y_test, order_penalty=None, tolerance=None,
                   task_weight=None, use_gpu=True, encoding='utf-8', verbose=False):
        start_time = time.time()
        x_test = torch.LongTensor(self.sentences_to_sequences(x_test, encoding=encoding))
        formatted_y_test = torch.LongTensor(
            utility.OrdinalLabelReformat(y_test, self.hyper_param['num_target_classes']))

        # Allow certain hyperparameters to be customized during testing. Without
        # customization, default to the ones used during training.
        if order_penalty is None:
            order_penalty = self.hyper_param['order_penalty']
        if tolerance is None:
            tolerance = self.hyper_param['cumulative_score_tolerance']
        if task_weight is None:
            task_weight = self.task_weight

        test_loss, test_mae, test_cs, _, _ = OrdinalRNNModelEval(
            model=self.model, x_val=x_test, y_val=y_test,
            num_target_classes=self.hyper_param['num_target_classes'],
            formatted_y_val=formatted_y_test,
            max_eval_batch_size=self.hyper_param['max_eval_batch_size'],
            use_gpu=use_gpu, tolerance=tolerance, task_weight=task_weight,
            order_penalty=order_penalty,
            ordinal_reg_type=self.hyper_param['ordinal_reg_type'])

        if verbose:
            print('time for testing: {:5.2f} min | ordinal regression loss: {:5.2f} | '
                  'mean absolute error: {:5.3f} | cumulative score: {:5.3f}'.format(
                (time.time() - start_time) / 60, test_loss, test_mae, test_cs))

        return test_loss, test_mae, test_cs

    def save_model(self, save_dir_path):
        if not os.path.exists(save_dir_path):
            os.makedirs(save_dir_path)
        # Model needs to be moved to cpu before saving.
        self.model.cpu()

        # The recommended way to save a PyTorch model is to save the hyperparameters
        # and model weights separately. We'll need to save the word2id as well.
        weights_file_path = os.path.join(
            save_dir_path, self.hyper_param['model_name'] + '_weights.pkl')
        hyper_param_file_path = os.path.join(
            save_dir_path, self.hyper_param['model_name'] + '_hyper_param.pkl')
        vocab_file_path = os.path.join(
            save_dir_path, self.hyper_param['model_name'] + '_vocab.pkl')
        with open(weights_file_path, 'wb') as f:
            torch.save(self.model.state_dict(), f)
        with open(hyper_param_file_path, 'wb') as f:
            pickle.dump(self.hyper_param, f)
        with open(vocab_file_path, 'wb') as f:
            pickle.dump(self.word2id, f)

    def load_model(self, model_name, load_dir_path):
        weights_file_path = os.path.join(
            load_dir_path, model_name + '_weights.pkl')
        hyper_param_file_path = os.path.join(
            load_dir_path, model_name + '_hyper_param.pkl')
        vocab_file_path = os.path.join(
            load_dir_path, model_name + '_vocab.pkl')
        # Override the hyper_param dictionary
        with open(hyper_param_file_path, 'rb') as f:
            self.hyper_param = pickle.load(f)
        # Re-initialize model, then load the saved weights
        self.initialize_model()
        with open(weights_file_path, 'rb') as f:
            self.model.load_state_dict(torch.load(f))
        # Load the word2id
        with open(vocab_file_path, 'rb') as f:
            self.word2id = pickle.load(f)

    def predict(self, features, use_gpu=True, save_result_file_path=None,
                input_length=None, encoding='utf-8'):
        id_matrix = self.sentences_to_sequences(features, input_length=input_length,
                                  encoding=encoding)
        self.model.eval()
        if use_gpu:
            self.model.cuda()
        else:
            self.model.cpu()
        # Make prediction in batches to avoid having memory issue with large data
        batch_size = self.hyper_param['max_eval_batch_size']
        num_batch = int(np.ceil(id_matrix.shape[0] / float(batch_size)))
        for i in xrange(num_batch):
            # Last batch needs to be handled slightly differently
            if i == num_batch - 1:
                input_batch = torch.LongTensor(id_matrix[i * batch_size:])
            else:
                input_batch = torch.LongTensor(
                    id_matrix[i * batch_size:(i + 1) * batch_size])
            if use_gpu:
                input_batch = input_batch.cuda()
            initial_states = self.model.initialize_states(input_batch.size(0))
            batch_pred_prob = self.model(
                Variable(input_batch), initial_states, True).data
            if i == 0:
                pred_prob = batch_pred_prob
            else:
                if self.hyper_param['ordinal_reg_type'] == 'separate':
                    pred_prob = torch.cat([pred_prob, batch_pred_prob], dim=1)
                else:
                    pred_prob = torch.cat([pred_prob, batch_pred_prob], dim=0)

        # Convert predicted probabilities to predicted labels
        pred_prob = pred_prob.cpu().numpy()
        if self.hyper_param['ordinal_reg_type'] == 'separate':
            pred_prob = utility.Softmax(pred_prob)
        pred_label = utility.Prob2Label(pred_prob,
                                        self.hyper_param['ordinal_reg_type'])

        if save_result_file_path is not None:
            df = pd.DataFrame(data={'text': features, 'prediction': pred_label})
            df.to_csv(save_result_file_path)

        return pred_prob, pred_label

    def word_by_word_predict(self, input_string, use_gpu=True, encoding='utf-8'):
        """
        Obtains prediction word by word for a single sentence, i.e., for all sub-
        sequences of the sentence that start from the beginning. Useful for examining
        how prediction changes as sentence goes on.
        :param input_string: Text string.
        :param use_gpu: Whether gpu is used.
        :return word_list: List of words in the input_string.
        :return pred_prob: Predicted probabilities, where the i-th row is the
        prediction for the sentence up to the i-th word.
        :return pred_label: Predicted labels.
        """
        word_list = input_string.split()
        seq_list = []
        # Construct all sub-sequences that start from the beginning of the sentence.
        for i in xrange(len(word_list)):
            sub_sequence = ' '.join(word_list[:i + 1])
            seq_list.append(sub_sequence)

        # Treat every sub-sequence as a separate sample in test data, and feed into
        # predict(). There're more efficient ways but not worth the dev effort.
        pred_prob, pred_label = self.predict(features=pd.Series(seq_list),
                                             use_gpu=use_gpu,
                                             input_length=len(word_list),
                                             encoding=encoding)

        return word_list, pred_prob, pred_label


if __name__ == '__main__':
    from GFRNN_config import config

    if config.rep > 1:
        val_mae_all = np.zeros(config.rep)
        val_cs_all = np.zeros(config.rep)
        test_mae_all = np.zeros(config.rep)
        test_cs_all = np.zeros(config.rep)
        binary_test_acc = np.zeros(config.rep)
        binary_test_weighted_acc = np.zeros(config.rep)

    for iteration in xrange(config.rep):
        # Increment seed every iteration.
        seed = config.seed + iteration

        # Load data
        data = pd.read_pickle('data/trafficking10k_cleaned.pkl')
        if config.join_title_body:
            data['raw'] = data['title'] + ' ' + data['raw']
        train_dat, valid_dat, test_dat = utility.DataSplit(data, config.train_frac,
                                                           config.valid_frac, seed,
                                                           False)

        # Train model
        ord_rnn_classifier = OrdinalRNNClassifier(
            num_target_classes=config.num_target_classes,
            embedding_vector_length=config.embedding_vector_length,
            rnn_type=config.rnn_type, dropout=config.dropout, init_lr=config.init_lr,
            grad_clip=config.grad_clip, l2_penalty=config.l2_penalty,
            max_eval_batch_size=config.max_eval_batch_size, max_epoch=config.max_epoch,
            decay_factor=config.decay_factor, lr_decay_patience=config.lr_decay_patience,
            early_stop_patience=config.early_stop_patience, batch_size=config.batch_size,
            seed=seed, verbose=config.verbose, use_gpu=config.use_gpu,
            output_layer_type=config.output_layer_type, model_name=config.model_name,
            hidden_size=config.hidden_size, rnn_layer_num=config.rnn_layer_num,
            batch_normalization=config.batch_normalization,
            residual_connection=config.residual_connection,
            input_length=config.input_length, embedding_lr_scale=config.embedding_lr_scale,
            min_frequency=config.min_frequency, ordinal_reg_type=config.ordinal_reg_type,
            task_weight_scheme=config.task_weight_scheme,
            cumulative_score_tolerance=config.cumulative_score_tolerance,
            order_penalty=config.order_penalty,
            equalize_class_size=config.equalize_class_size,
            pretrain_vocab_file_path=config.pretrain_vocab_file_path,
            pretrain_embedding_file_path=config.pretrain_embedding_file_path)
        ord_rnn_classifier.build_vocabulary(train_dat['raw'])
        ord_rnn_classifier.initialize_model()
        ord_rnn_classifier.train_model(
            x_train=train_dat['raw'], y_train=train_dat['label'].values,
            x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

        # Evaluate model
        val_loss, val_mae, val_cs = ord_rnn_classifier.test_model(
            x_test=valid_dat['raw'], y_test=valid_dat['label'].values,
            use_gpu=config.use_gpu)
        test_loss, test_mae, test_cs = ord_rnn_classifier.test_model(
            x_test=test_dat['raw'], y_test=test_dat['label'].values,
            use_gpu=config.use_gpu)

        print 'Validation loss: %5.3f | Validation mean abs error: %5.3f | Validation ' \
              'cumulative score: %0.3f | Test loss: %5.3f | Test mean abs error: %5.3f' \
              ' | Test cumulative score: %0.3f' % (val_loss, val_mae, val_cs, test_loss,
                                                   test_mae, test_cs)

        # Check out the confusion matrix for test data
        pred_prob, pred_label = ord_rnn_classifier.predict(test_dat['raw'])
        if config.verbose:
            print confusion_matrix(test_dat['label'].values, pred_label)

        # Check out the binary classification performance of the ordinal regression model
        pred_label = utility.GroupLabels(pred_label, cutoff=3, verbose=False)
        truth = utility.GroupLabels(test_dat['label'], cutoff=3, verbose=False)
        test_acc = np.mean(pred_label == truth)
        test_weighted_acc = utility.WeightedACC(pred_label, truth)
        print 'Binary classification test_acc: %5.3f | test_weighted_acc: %5.3f' % (
            test_acc, test_weighted_acc
        )

        # Save model
        if config.save_model:
            ord_rnn_classifier.save_model(config.save_dir_path)

        if config.rep > 1:
            val_mae_all[iteration] = val_mae
            val_cs_all[iteration] = val_cs
            test_mae_all[iteration] = test_mae
            test_cs_all[iteration] = test_cs
            binary_test_acc[iteration] = test_acc
            binary_test_weighted_acc[iteration] = test_weighted_acc

    if config.rep > 1:
        np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
        print '\nSeeds:', range(config.seed, config.seed + config.rep), \
            '\nval_mae_all:', val_mae_all, \
            '\nmean: %5.3f' % val_mae_all.mean(), \
            'stderr: %5.3f' % (val_mae_all.std() / config.rep**0.5), \
            '\nval_cs_all:', val_cs_all, \
            '\nmean: %5.3f' % val_cs_all.mean(), \
            'stderr: %5.3f' % (val_cs_all.std() / config.rep**0.5), \
            '\ntest_mae_all:', test_mae_all, \
            '\nmean: %5.3f' % test_mae_all.mean(), \
            'stderr: %5.3f' % (test_mae_all.std() / config.rep**0.5), \
            '\ntest_cs_all:', test_cs_all, \
            '\nmean: %5.3f' % test_cs_all.mean(), \
            'stderr: %5.3f' % (test_cs_all.std() / config.rep**0.5), \
            '\nbinary_test_acc:', binary_test_acc, \
            '\nmean: %5.3f' % binary_test_acc.mean(), \
            'stderr: %5.3f' % (binary_test_acc.std() / config.rep**0.5), \
            '\nbinary_test_weighted_acc:', binary_test_weighted_acc, \
            '\nmean: %5.3f' % binary_test_weighted_acc.mean(), \
            'stderr: %5.3f' % (binary_test_weighted_acc.std() / config.rep**0.5)