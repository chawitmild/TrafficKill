"""
This file implements Gated Feedback Recurrent Neural Network classification model
based on the paper by Chung et al (https://arxiv.org/pdf/1502.02367.pdf) using
PyTorch.
"""
import os
import abc
import sys
import math
import time
import copy
import torch
import utility
import numpy as np
import numpy.random as nr
import pandas as pd
import cPickle as pickle
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from torch.nn.modules.rnn import RNNCellBase
from torch.nn.parameter import Parameter
from torch.utils.data import DataLoader, TensorDataset
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import confusion_matrix
from restricted_embedding import construct_restricted_embedding


class GFRUCell(RNNCellBase):
    def __init__(self, input_size, hidden_size, layer_num, add_hidden_layer_bias=True):
        super(GFRUCell, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.layer_num = layer_num

        self.lstm_weight_input_to_hidden = Parameter(torch.Tensor(3 * hidden_size, input_size))
        self.lstm_weight_hidden_to_hidden = Parameter(torch.Tensor(3 * hidden_size, hidden_size))
        if add_hidden_layer_bias:
            self.lstm_bias_input_to_hidden = Parameter(torch.Tensor(3 * hidden_size))
            self.lstm_bias_hidden_to_hidden = Parameter(torch.Tensor(3 * hidden_size))
        else:
            self.register_parameter('bias_input_to_hidden', None)
            self.register_parameter('bias_hidden_to_hidden', None)

        self.global_reset_weight_input_to_hidden = Parameter(torch.Tensor(layer_num, input_size))
        self.global_reset_weight_hidden_to_hidden = Parameter(torch.Tensor(layer_num, layer_num * hidden_size))
        self.cell_weight_input_to_hidden = Parameter(torch.Tensor(hidden_size, input_size))
        self.cell_weight_hidden_to_hidden = Parameter(torch.Tensor(layer_num * hidden_size, layer_num * hidden_size))
        self.initialize_parameters()

    def initialize_parameters(self):
        stdv = 1.0 / math.sqrt(self.hidden_size)
        for weight in self.parameters():
            weight.data.uniform_(-stdv, stdv)

    def forward(self, input_tensor, prev_cell_state, prev_hidden_state, prev_hidden_state_stack):
        lstm_gates = F.sigmoid(F.linear(input_tensor, self.lstm_weight_input_to_hidden,
                                        self.lstm_bias_input_to_hidden) +
                               F.linear(prev_hidden_state, self.lstm_weight_hidden_to_hidden,
                                        self.lstm_bias_hidden_to_hidden))

        in_gate, forget_gate, out_gate = lstm_gates.chunk(3, 1)

        global_reset_gate = F.sigmoid(F.linear(input_tensor, self.global_reset_weight_input_to_hidden) +
                                      F.linear(prev_hidden_state_stack, self.global_reset_weight_hidden_to_hidden))

        cell_gate_input_to_hidden = F.linear(input_tensor, self.cell_weight_input_to_hidden)
        cell_gate_hidden_to_hidden_all = F.linear(prev_hidden_state_stack, self.cell_weight_hidden_to_hidden)
        for layer_index in xrange(self.layer_num):
            summand = cell_gate_hidden_to_hidden_all[:,
                      (self.hidden_size * layer_index):(self.hidden_size * (layer_index + 1))] * \
                      global_reset_gate[:, layer_index].contiguous().view(-1, 1).expand_as(cell_gate_input_to_hidden)
            if layer_index == 0:
                cell_gate_hidden_to_hidden = summand
            else:
                cell_gate_hidden_to_hidden += summand

        cell_gate = F.tanh(cell_gate_input_to_hidden + cell_gate_hidden_to_hidden)

        next_cell_state = forget_gate * prev_cell_state + in_gate * cell_gate
        next_hidden_state = out_gate * F.tanh(next_cell_state)

        return next_cell_state, next_hidden_state


class GFRUStack(nn.Module):
    def __init__(self, input_size, hidden_size, layer_num, add_hidden_layer_bias=True, dropout=0,
                 residual_connection=True):
        super(GFRUStack, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.layer_num = layer_num
        self.drop = nn.Dropout(dropout)
        self.residual_connection = residual_connection
        gfru_cell_list = [None] * layer_num
        for layer_index in xrange(layer_num):
            if layer_index == 0:
                gfru_cell_list[layer_index] = GFRUCell(input_size, hidden_size, layer_num, add_hidden_layer_bias)
            else:
                gfru_cell_list[layer_index] = GFRUCell(hidden_size, hidden_size, layer_num, add_hidden_layer_bias)

        self.gfru_cell_list = nn.ModuleList(gfru_cell_list)

    def forward(self, input_tensor, all_prev_cell_states, all_prev_hidden_states):
        # The following line works for pytorch v0.2 but not v0.3. In v0.3 torch.cat
        # no longer allows a single Variable as input. Need to add list().
        # prev_hidden_state_stack = torch.cat(all_prev_hidden_states, dim=1)
        prev_hidden_state_stack = torch.cat(list(all_prev_hidden_states), dim=1)
        batch_size = all_prev_hidden_states[0].size(0)
        all_next_cell_states = [None] * self.layer_num
        all_next_hidden_states = [None] * self.layer_num
        for layer_index in xrange(self.layer_num):
            if layer_index == 0:
                layer_input_tensor = input_tensor
            elif layer_index == 1:
                if self.residual_connection and self.input_size == self.hidden_size:
                    layer_input_tensor = all_next_hidden_states[layer_index - 1] + input_tensor
                else:
                    layer_input_tensor = all_next_hidden_states[layer_index - 1]
            else:
                if self.residual_connection:
                    layer_input_tensor = all_next_hidden_states[layer_index - 1] + \
                                         all_next_hidden_states[layer_index - 2]
                else:
                    layer_input_tensor = all_next_hidden_states[layer_index - 1]

            all_next_cell_states[layer_index], all_next_hidden_states[layer_index] = \
                self.gfru_cell_list[layer_index](self.drop(layer_input_tensor), all_prev_cell_states[layer_index],
                                                 all_prev_hidden_states[layer_index], prev_hidden_state_stack)

        all_next_cell_states = torch.cat(all_next_cell_states, 0).view(self.layer_num, batch_size, self.hidden_size)
        all_next_hidden_states = torch.cat(all_next_hidden_states, 0).view(self.layer_num, batch_size, self.hidden_size)

        return all_next_cell_states, all_next_hidden_states


class GFRUNetwork(nn.Module):
    def __init__(self, input_size, hidden_size, layer_num, bias=True, dropout=0, residual_connection=True):
        super(GFRUNetwork, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.layer_num = layer_num
        self.bias = bias
        self.drop = nn.Dropout(dropout)
        self.gfru_stack = GFRUStack(input_size, hidden_size, layer_num, bias, dropout, residual_connection)

    def forward(self, input_tensor, prev_states):
        sequence_length = input_tensor.size(0)
        batch_size = input_tensor[0].size(0)
        all_prev_hidden_states, all_prev_cell_states = prev_states
        output_tensor = []
        for word_index in range(sequence_length):
            all_prev_cell_states, all_prev_hidden_states = \
                self.gfru_stack(input_tensor[word_index], all_prev_cell_states, all_prev_hidden_states)
            output_tensor.append(all_prev_hidden_states[-1])

        output_tensor = torch.cat(output_tensor, 0).view(sequence_length, batch_size, self.hidden_size)
        final_states = (all_prev_hidden_states, all_prev_cell_states)

        return output_tensor, final_states


class RNNModel(nn.Module):
    def __init__(self, token_num, embedding_vector_length=128, hidden_size=128, rnn_layer_num=3, rnn_type='GFRU',
                 num_target_classes=3, dropout=0.5, batch_normalization=True, output_layer_type='last',
                 residual_connection=True):
        super(RNNModel, self).__init__()
        self.drop = nn.Dropout(dropout)
        self.token_num = token_num
        self.encoder = nn.Embedding(self.token_num, embedding_vector_length)
        if rnn_type == 'GFRU':
            self.rnn = GFRUNetwork(embedding_vector_length, hidden_size, rnn_layer_num, dropout=dropout,
                                   residual_connection=residual_connection)
        elif rnn_type == 'LSTM':
            self.rnn = nn.LSTM(embedding_vector_length, hidden_size, rnn_layer_num, dropout=dropout)
        else:
            raise ValueError("Invalid rnn type. Choices are 'GFRU' and 'LSTM'.")

        self.classifier = nn.Linear(hidden_size, num_target_classes)
        self.embedding_vector_length = embedding_vector_length
        self.hidden_size = hidden_size
        self.rnn_layer_num = rnn_layer_num
        self.batch_normalization = batch_normalization
        self.batch_norm_embedding_vec = nn.BatchNorm1d(embedding_vector_length)
        self.batch_norm_rnn_output = nn.BatchNorm1d(hidden_size)
        self.output_layer_type = output_layer_type
        if output_layer_type not in ['last', 'mean-pool']:
            raise ValueError("Invalid output layer type. Choices are 'last' and 'mean-pool'.")

        self.initialize_parameters()

    def initialize_parameters(self):
        self.encoder.weight.data.uniform_(-0.1, 0.1)
        self.classifier.bias.data.fill_(0)
        self.classifier.weight.data.uniform_(-0.1, 0.1)

    def forward(self, input_tensor, prev_states, batch_first=True):
        if batch_first:
            input_tensor = input_tensor.t().contiguous()

        embedding = self.drop(self.encoder(input_tensor))

        if self.batch_normalization:
            embedding = self.batch_norm_embedding_vec(
                embedding.permute(1, 2, 0).contiguous()).permute(2, 0, 1)

        output_tensor, final_states = self.rnn(embedding, prev_states)

        if self.output_layer_type == 'last':
            output_tensor = self.drop(output_tensor[-1])

        elif self.output_layer_type == 'mean-pool':
            output_tensor = self.drop(torch.squeeze(torch.mean(output_tensor, dim=0)))

        if self.batch_normalization:
            output_tensor = self.batch_norm_rnn_output(output_tensor)
        prediction = self.classifier(output_tensor)

        return prediction, final_states

    def initialize_states(self, batch_size):
        weight = next(self.parameters()).data

        return Variable(weight.new(self.rnn_layer_num, batch_size, self.hidden_size).zero_()), \
               Variable(weight.new(self.rnn_layer_num, batch_size, self.hidden_size).zero_())


def RNNModelEval(model, x_val, y_val, criterion=nn.CrossEntropyLoss(), max_eval_batch_size=1500, use_gpu=False):
    model.eval()
    if use_gpu:
        model.cuda()
    else:
        model.cpu()
    xy_val = TensorDataset(torch.LongTensor(x_val), torch.LongTensor(y_val))
    eval_loader = DataLoader(xy_val, max_eval_batch_size, shuffle=False, drop_last=False)
    for i, xy_val_batch in enumerate(eval_loader, 0):

        x_val_batch, y_val_batch = xy_val_batch
        if use_gpu:
            x_val_batch, y_val_batch = x_val_batch.cuda(), y_val_batch.cuda()
        current_eval_batch_size = x_val_batch.size(0)
        initial_states = model.initialize_states(current_eval_batch_size)
        batch_output_tensor, _ = model(Variable(x_val_batch), initial_states, True)
        # Always store the data and not Variable whenever possible, otherwise the
        # compute graph will keep growing. Without the next line, memory will blow up
        # for large x_val no matter how small max_eval_batch_size is.
        batch_output_tensor = batch_output_tensor.data
        if i == 0:
            output_tensor = batch_output_tensor
        else:
            output_tensor = torch.cat([output_tensor, batch_output_tensor], dim=0)

    if use_gpu:
        output_tensor = output_tensor.cpu()
    loss = criterion(Variable(output_tensor), Variable(y_val)).data.numpy()[0]
    accuracy = (torch.max(output_tensor, 1)[1].view(-1) == y_val).sum() / float(len(x_val))

    return loss, accuracy, output_tensor


def RNNModelTrain(model, xy_train, lr=3.0, criterion=nn.CrossEntropyLoss(), batch_size=200, l2_penalty=0,
                  grad_clip=0.25, use_gpu=False, embedding_lr_scale=1):
    model.train()
    if use_gpu:
        model.cuda()
    train_loader = DataLoader(xy_train, batch_size, shuffle=True, drop_last=True)

    if embedding_lr_scale == 1:
        optimizer = optim.SGD(model.parameters(), lr=lr, weight_decay=l2_penalty)
    else:
        embedding_params_list = list(map(id, model.encoder.parameters()))
        non_embedding_params = filter(lambda p: id(p) not in embedding_params_list, model.parameters())
        optimizer = optim.SGD([{'params': non_embedding_params},
                               {'params': model.encoder.parameters(), 'lr': lr * embedding_lr_scale}],
                              lr=lr, weight_decay=l2_penalty)

    avg_batch_loss = 0
    avg_batch_accuracy = 0
    for i, xy_train_batch in enumerate(train_loader, 0):

        x_train_batch, y_train_batch = xy_train_batch
        if use_gpu:
            x_train_batch, y_train_batch = x_train_batch.cuda(), y_train_batch.cuda()
        initial_states = model.initialize_states(batch_size)

        optimizer.zero_grad()
        output_tensor, _ = model(Variable(x_train_batch), initial_states)
        loss = criterion(output_tensor, Variable(y_train_batch))
        loss.backward()

        torch.nn.utils.clip_grad_norm(model.parameters(), grad_clip)

        optimizer.step()
        if use_gpu:
            avg_batch_loss += loss.data.cpu().numpy()[0] * batch_size
        else:
            avg_batch_loss += loss.data.numpy()[0] * batch_size
        avg_batch_accuracy += (torch.max(output_tensor.data, 1)[1].view(-1) == y_train_batch).sum()

    avg_batch_loss /= (i + 1) * batch_size
    avg_batch_accuracy /= (i + 1) * float(batch_size)

    return model, avg_batch_loss, avg_batch_accuracy


class RNNClassifier:
    def __init__(self, num_target_classes=7, embedding_vector_length=128,
                 rnn_type='GFRU', dropout=0.2, init_lr=3.0, grad_clip=0.25,
                 l2_penalty=0.00001, max_eval_batch_size=500, max_epoch=50,
                 decay_factor=2.0, lr_decay_patience=3, early_stop_patience=9,
                 batch_size=200, seed=0, verbose=False, use_gpu=True,
                 output_layer_type='mean-pool', model_name='gfrnn', hidden_size=128,
                 rnn_layer_num=3, batch_normalization=True, residual_connection=True,
                 input_length=120, embedding_lr_scale=1.0, min_frequency=2,
                 pretrain_vocab_file_path=None, pretrain_embedding_file_path=None):
        self.hyper_param = \
            {'embedding_vector_length': embedding_vector_length, 'hidden_size':
             hidden_size, 'rnn_layer_num': rnn_layer_num, 'rnn_type': rnn_type,
             'dropout': dropout, 'init_lr': init_lr, 'grad_clip': grad_clip,
             'l2_penalty': l2_penalty, 'max_eval_batch_size': max_eval_batch_size,
             'max_epoch': max_epoch, 'decay_factor': decay_factor,
             'lr_decay_patience': lr_decay_patience, 'early_stop_patience':
             early_stop_patience, 'batch_size': batch_size, 'seed': seed, 'verbose':
             verbose, 'use_gpu': use_gpu, 'output_layer_type': output_layer_type,
             'model_name': model_name, 'num_target_classes': num_target_classes,
             'batch_normalization': batch_normalization, 'residual_connection':
             residual_connection, 'input_length': input_length, 'embedding_lr_scale':
             embedding_lr_scale, 'min_frequency': min_frequency,
             'pretrain_vocab_file_path': pretrain_vocab_file_path,
             'pretrain_embedding_file_path': pretrain_embedding_file_path}

        # If config.min_frequency >= 1, it represents the minimum number of docs a
        # word needs to be in, otherwise it represents the percentage.
        if min_frequency >= 1:
            self.hyper_param['min_frequency'] = int(min_frequency)

        self.word2id = None

        nr.seed(self.hyper_param['seed'])
        torch.manual_seed(self.hyper_param['seed'])
        if self.hyper_param['use_gpu']:
            torch.cuda.manual_seed(self.hyper_param['seed'])

    def initialize_model(self):
        # Note that this step needs to be done after build_vocabulary(), since we need
        # token_num when specifying the embedding layer (and hence the model).
        self.model = \
            RNNModel(**dict((key, self.hyper_param[key]) for key in
                            ('token_num', 'embedding_vector_length', 'hidden_size',
                             'rnn_layer_num', 'rnn_type', 'num_target_classes',
                             'dropout', 'batch_normalization', 'output_layer_type',
                             'residual_connection') if key in self.hyper_param))

        # Load pretrained embedding if file paths were provided
        if self.hyper_param['pretrain_vocab_file_path'] is not None:
            # Load vocabulary used in pretraining
            pretrain_vocab = pd.read_csv(self.hyper_param['pretrain_vocab_file_path'],
                                         header=None, sep=' ')
            # pretrain_vocab produced by current word2vec implementation will have
            # 2 columns, 1st for words where their order matches the embedding, and
            # 2nd for frequency counts which we don't need.
            pretrain_id2word = pretrain_vocab[0].tolist()
            pretrain_word2id = {}
            for i, word in enumerate(pretrain_id2word):
                # Manually fix a small technical issue: pandas reads 'null' from the
                # text file as nan, which will cause problems.
                if word is np.NaN:
                    word = 'null'
                pretrain_word2id[word] = i
            # Load pretrained embedding
            pretrain_embedding = np.loadtxt(self.hyper_param['pretrain_embedding_file_path'])
            assert self.hyper_param['embedding_vector_length'] == pretrain_embedding.shape[1]
            # Restrict the pretrained embedding to the word2id from the current training
            # set. Remember to adjust the encoding here if the codes for constructing the
            # word2id's change.
            restricted_embedding = construct_restricted_embedding(
                restricted_word2id=self.word2id, pretrain_word2id=pretrain_word2id,
                pretrain_embedding=pretrain_embedding, restricted_word2id_encoding=None,
                pretrain_word2id_encoding='utf-8')
            # Replace the randomly initialized embedding in the model with the constructed
            # embedding.
            self.model.encoder.weight.data = torch.Tensor(restricted_embedding)
            print 'Successfully imported pretrained embedding'

        print self.hyper_param

    def build_vocabulary(self, features, encoding='utf-8'):
        # We don't need the vectorizer, only the word2id that goes with it.
        vectorizer = CountVectorizer(min_df=self.hyper_param['min_frequency'],
                                     tokenizer=lambda doc: doc.lower().split(" "),
                                     encoding=encoding)
        vectorizer.fit(features)
        self.word2id = vectorizer.vocabulary_
        # +1 for the token for unknown. Refer to sentences_to_sequences().
        self.hyper_param['token_num'] = len(self.word2id) + 1

    def sentences_to_sequences(self, features, input_length=None, encoding='utf-8'):
        # Default input_length comes from self.hyper_param
        if input_length is None:
            input_length = self.hyper_param['input_length']

        id_matrix = np.zeros(shape=(features.shape[0], input_length),
                             dtype=int)
        unknown_word_count = 0
        total_word_count = 0
        for sent_pos, sentence in enumerate(features):
            # Truncate long sentences at max_length
            words = sentence.split()[:input_length]
            id_vec = np.zeros(len(words), dtype=int)
            total_word_count += len(words)
            for word_pos, word in enumerate(words):
                # Decode to unicode if the input has an encoding
                if encoding is not None:
                    word = word.decode(encoding)
                if word not in self.word2id:
                    # Map all unknown words to a single id. Note that len(word2id)
                    # is not in word2id.values().
                    id_vec[word_pos] = len(self.word2id)
                    unknown_word_count += 1
                else:
                    id_vec[word_pos] = self.word2id.get(word)
            # Note that the following line ensures that shorter sentence will be 0-padded
            # in the beginning.
            id_matrix[sent_pos, input_length - len(words):] = id_vec

        if self.hyper_param['verbose']:
            print "Unknown word ratio: {:5.3f}".format(float(unknown_word_count) / total_word_count)

        return id_matrix

    def train_model(self, x_train, y_train, x_val, y_val, encoding='utf-8'):
        # Wrap data in PyTorch tensors. Note that all the input are expected to be
        # numpy arrays. So if y_train is a pandas series, use y_train.values.
        x_train = torch.LongTensor(self.sentences_to_sequences(x_train, encoding=encoding))
        x_val = torch.LongTensor(self.sentences_to_sequences(x_val, encoding=encoding))
        y_train = torch.LongTensor(y_train)
        y_val = torch.LongTensor(y_val)

        # Define the loss function
        criterion = nn.CrossEntropyLoss()
        lr = self.hyper_param['init_lr']

        # Create training batch generator
        xy_train = TensorDataset(x_train, y_train)

        # Perform training until reaching max_epoch, early stopping, or user interruption
        start_time = time.time()
        best_val_accuracy = None
        model = copy.deepcopy(self.model)
        try:
            for epoch in range(self.hyper_param['max_epoch']):
                model, train_loss, train_accuracy = RNNModelTrain(model, xy_train, lr, criterion,
                                                                  self.hyper_param['batch_size'],
                                                                  self.hyper_param['l2_penalty'],
                                                                  self.hyper_param['grad_clip'],
                                                                  self.hyper_param['use_gpu'],
                                                                  self.hyper_param['embedding_lr_scale'])
                val_loss, val_accuracy, _ = RNNModelEval(model, x_val, y_val, criterion,
                                                         self.hyper_param['max_eval_batch_size'],
                                                         self.hyper_param['use_gpu'])
                if self.hyper_param['verbose']:
                    print('| end of epoch: {:3d} | time passed: {:5.2f} min | training loss: {:5.2f} | '
                          'training accuracy: {:5.3f} | validation loss: {:5.2f} | validation accuracy: {:5.3f} | '
                          'learning rate: {:5.5f}'.format(epoch, (time.time() - start_time) / 60, train_loss,
                                                          train_accuracy, val_loss, val_accuracy, lr))
                # If best validation accuracy is achieved in the current epoch, checkpoint the model and reset learning
                # rate decay and early stopping counter.
                if not best_val_accuracy or val_accuracy > best_val_accuracy:
                    best_val_accuracy = val_accuracy
                    self.model = copy.deepcopy(model)
                    lr_decay_counter = 0
                    early_stop_counter = 0
                else:
                    lr_decay_counter += 1
                    early_stop_counter += 1

                # Implement learning rate decay
                # This schema decays learning rate based on validation accuracy
                if lr_decay_counter == self.hyper_param['lr_decay_patience']:
                    lr /= self.hyper_param['decay_factor']
                    lr_decay_counter = 0

                # Invoke early termination if training plateaus
                if early_stop_counter == self.hyper_param['early_stop_patience']:
                    break

        # Also allow user to manually terminate
        except KeyboardInterrupt:
            print('Exiting from training early')

    def save_model(self, save_dir_path):
        if not os.path.exists(save_dir_path):
            os.makedirs(save_dir_path)
        # Model needs to be moved to cpu before saving.
        self.model.cpu()

        # The recommended way to save a PyTorch model is to save the hyperparameters
        # and model weights separately. We'll need to save the word2id as well.
        weights_file_path = os.path.join(
            save_dir_path, self.hyper_param['model_name'] + '_weights.pkl')
        hyper_param_file_path = os.path.join(
            save_dir_path, self.hyper_param['model_name'] + '_hyper_param.pkl')
        vocab_file_path = os.path.join(
            save_dir_path, self.hyper_param['model_name'] + '_vocab.pkl')
        with open(weights_file_path, 'wb') as f:
            torch.save(self.model.state_dict(), f)
        with open(hyper_param_file_path, 'wb') as f:
            pickle.dump(self.hyper_param, f)
        with open(vocab_file_path, 'wb') as f:
            pickle.dump(self.word2id, f)

    def load_model(self, model_name, load_dir_path):
        weights_file_path = os.path.join(
            load_dir_path, model_name + '_weights.pkl')
        hyper_param_file_path = os.path.join(
            load_dir_path, model_name + '_hyper_param.pkl')
        vocab_file_path = os.path.join(
            load_dir_path, model_name + '_vocab.pkl')
        # Override the hyper_param dictionary
        with open(hyper_param_file_path, 'rb') as f:
            self.hyper_param = pickle.load(f)
        # Re-initialize model, then load the saved weights
        self.initialize_model()
        with open(weights_file_path, 'rb') as f:
            self.model.load_state_dict(torch.load(f))
        # Load the word2id
        with open(vocab_file_path, 'rb') as f:
            self.word2id = pickle.load(f)

    def predict(self, features, use_gpu=True, save_result_file_path=None,
                input_length=None, encoding='utf-8'):
        id_matrix = self.sentences_to_sequences(features, input_length=input_length,
                                                encoding=encoding)
        self.model.eval()
        if use_gpu:
            self.model.cuda()
        else:
            self.model.cpu()
        # Make prediction in batches to avoid having memory issue with large data
        batch_size = self.hyper_param['max_eval_batch_size']
        num_batch = int(np.ceil(id_matrix.shape[0] / float(batch_size)))
        prediction = torch.Tensor(0)
        for i in xrange(num_batch):
            # Last batch needs to be handled slightly differently
            if i == num_batch - 1:
                input_batch = torch.LongTensor(id_matrix[i * batch_size:])
            else:
                input_batch = torch.LongTensor(
                    id_matrix[i * batch_size:(i + 1) * batch_size])
            if use_gpu:
                input_batch = input_batch.cuda()
            initial_states = self.model.initialize_states(input_batch.size(0))
            batch_prediction, _ = self.model(Variable(input_batch), initial_states, True)
            prediction = torch.cat([prediction, batch_prediction.data.cpu()], dim=0)

        # Apply softmax to get probabilities and convert to labels
        prediction = prediction.numpy()
        pred_prob = np.exp(prediction) / np.sum(np.exp(prediction), axis=1).reshape(-1, 1)
        pred_label = np.argmax(pred_prob, axis=1)

        if save_result_file_path is not None:
            df = pd.DataFrame(data={'text': features, 'pred_prob': pred_prob,
                                    'pred_label': pred_label})
            df.to_csv(save_result_file_path)

        return pred_prob, pred_label

    def word_by_word_predict(self, input_string, use_gpu=True, encoding='utf-8'):
        """
        Obtains prediction word by word for a single sentence, i.e., for all sub-
        sequences of the sentence that start from the beginning. Useful for examining
        how prediction changes as sentence goes on.
        :param input_string: Text string.
        :param use_gpu: Whether gpu is used.
        :return word_list: List of words in the input_string.
        :return pred_prob: Predicted probabilities, where the i-th row is the
        prediction for the sentence up to the i-th word.
        :return pred_label: Predicted labels.
        """
        word_list = input_string.split()
        seq_list = []
        # Construct all sub-sequences that start from the beginning of the sentence.
        for i in xrange(len(word_list)):
            sub_sequence = ' '.join(word_list[:i + 1])
            seq_list.append(sub_sequence)

        # Treat every sub-sequence as a separate sample in test data, and feed into
        # predict(). There're more efficient ways but not worth the dev effort.
        pred_prob, pred_label = self.predict(features=pd.Series(seq_list),
                                             use_gpu=use_gpu,
                                             input_length=len(word_list),
                                             encoding=encoding)

        return word_list, pred_prob, pred_label


if __name__ == '__main__':
    from GFRNN_config import config

    if config.rep > 1:
        val_acc_all = np.zeros(config.rep)
        val_weighted_acc_all = np.zeros(config.rep)
        test_acc_all = np.zeros(config.rep)
        test_weighted_acc_all = np.zeros(config.rep)

    for iteration in xrange(config.rep):
        # Increment seed every iteration.
        seed = config.seed + iteration

        # Load data
        data = pd.read_pickle('data/trafficking10k_cleaned.pkl')
        if config.num_target_classes == 2:
            data['label'] = utility.GroupLabels(data['label'], cutoff=3, verbose=False)
        if config.join_title_body:
            data['raw'] = data['title'] + ' ' + data['raw']
        train_dat, valid_dat, test_dat = utility.DataSplit(data, config.train_frac,
                                                           config.valid_frac, seed,
                                                           False)

        # Train model
        rnn_classifier = RNNClassifier(
            num_target_classes=config.num_target_classes,
            embedding_vector_length=config.embedding_vector_length,
            rnn_type=config.rnn_type, dropout=config.dropout, init_lr=config.init_lr,
            grad_clip=config.grad_clip, l2_penalty=config.l2_penalty,
            max_eval_batch_size=config.max_eval_batch_size, max_epoch=config.max_epoch,
            decay_factor=config.decay_factor, lr_decay_patience=config.lr_decay_patience,
            early_stop_patience=config.early_stop_patience, batch_size=config.batch_size,
            seed=seed, verbose=config.verbose, use_gpu=config.use_gpu,
            output_layer_type=config.output_layer_type, model_name=config.model_name,
            hidden_size=config.hidden_size, rnn_layer_num=config.rnn_layer_num,
            batch_normalization=config.batch_normalization, residual_connection=config.residual_connection,
            input_length=config.input_length, embedding_lr_scale=config.embedding_lr_scale,
            min_frequency=config.min_frequency, pretrain_vocab_file_path=config.pretrain_vocab_file_path,
            pretrain_embedding_file_path=config.pretrain_embedding_file_path)
        rnn_classifier.build_vocabulary(train_dat['raw'])
        rnn_classifier.initialize_model()
        rnn_classifier.train_model(x_train=train_dat['raw'], y_train=train_dat['label'].values,
                                   x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

        # Evaluate model
        _, valid_pred = rnn_classifier.predict(valid_dat['raw'], use_gpu=config.use_gpu)
        valid_acc = np.mean(valid_pred == valid_dat['label'])
        valid_weighted_acc = utility.WeightedACC(valid_pred, valid_dat['label'])

        _, test_pred = rnn_classifier.predict(test_dat['raw'], use_gpu=config.use_gpu)
        test_acc = np.mean(test_pred == test_dat['label'])
        test_weighted_acc = utility.WeightedACC(test_pred, test_dat['label'])

        print 'Validation acc: %0.3f | Validation weighted acc: %0.3f | Test acc: ' \
              '%0.3f | Test weighted acc: %0.3f' % (valid_acc, valid_weighted_acc,
                                                    test_acc, test_weighted_acc)

        if config.verbose:
            print confusion_matrix(test_dat['label'].values, test_pred)

        # Save model
        if config.save_model:
            rnn_classifier.save_model(config.save_dir_path)

        if config.rep > 1:
            val_acc_all[iteration] = valid_acc
            val_weighted_acc_all[iteration] = valid_weighted_acc
            test_acc_all[iteration] = test_acc
            test_weighted_acc_all[iteration] = test_weighted_acc

    if config.rep > 1:
        np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
        print '\nSeeds:', range(config.seed, config.seed + config.rep), \
            '\nval_acc_all:', val_acc_all, \
            '\nmean: %5.3f' % val_acc_all.mean(), \
            'stderr: %5.3f' % (val_acc_all.std() / config.rep**0.5), \
            '\nval_weighted_acc_all:', val_weighted_acc_all, \
            '\nmean: %5.3f' % val_weighted_acc_all.mean(), \
            'stderr: %5.3f' % (val_weighted_acc_all.std() / config.rep**0.5), \
            '\ntest_acc_all:', test_acc_all, \
            '\nmean: %5.3f' % test_acc_all.mean(), \
            'stderr: %5.3f' % (test_acc_all.std() / config.rep**0.5), \
            '\ntest_weighted_acc_all:', test_weighted_acc_all, \
            '\nmean: %5.3f' % test_weighted_acc_all.mean(), \
            'stderr: %5.3f' % (test_weighted_acc_all.std() / config.rep**0.5)
