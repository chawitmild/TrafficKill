"""
Compare various models on Trafficking10k using cross-validation
"""
import argparse
import utility
import time
import numpy as np
import pandas as pd
import numpy.random as nr
from sklearn.model_selection import KFold
from GFRNN import RNNClassifier
from GFRNN_ord_reg import OrdinalRNNClassifier
from GFRNN_cont_reg import RNNRegression
from baseline_classification import BaselineClassifier

# Choose model to be evaluated from command line
parser = argparse.ArgumentParser()
parser.add_argument('--model',
                    help='model to be evaluated')
args = parser.parse_args()

# Load data
data = pd.read_pickle('data/trafficking10k_cleaned.pkl')
data['raw'] = data['title'] + ' ' + data['raw']
train_size = int(data['raw'].shape[0] * 0.7)

# Set CV fold. The number of folds will be fixed at 10. If changed, need to adjust
# train_size accordingly.
# A lesson learned after 2 days of debugging: data needs to be shuffled here. Even
# though KFold from sklearn has a shuffle argument, the generated train_index and
# test_index are SORTED, which could lead to distribution differences between train
# set and test set if the original data is not shuffled, deteriorating performance.
nr.seed(0)
data = data.sample(frac=1, random_state=0)
CV_index = KFold(n_splits=10, shuffle=True)
# Create placeholders for all metrics of interest
mae = np.zeros(10)
macro_mae = np.zeros(10)
cs0 = np.zeros(10)
cs1 = np.zeros(10)
bin_acc = np.zeros(10)
weighted_bin_acc = np.zeros(10)
i = 0
for train_index, test_index in CV_index.split(data):
    start_time = time.time()
    train_dat = data.iloc[train_index, :][:train_size]
    valid_dat = data.iloc[train_index, :][train_size:]
    test_dat = data.iloc[test_index, :]

    if args.model == 'gfrnn_classification':
        model = RNNClassifier(
            pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
            pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
        model.build_vocabulary(train_dat['raw'])
        model.initialize_model()
        model.train_model(
            x_train=train_dat['raw'], y_train=train_dat['label'].values,
            x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

        _, test_pred = model.predict(test_dat['raw'], use_gpu=True)
        mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
            utility.AllMetrics(test_pred, test_dat['label'].values)

    elif args.model == 'gfrnn_ord_reg':
        model = OrdinalRNNClassifier(
            order_penalty=1,
            pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
            pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
        model.build_vocabulary(train_dat['raw'])
        model.initialize_model()
        model.train_model(
            x_train=train_dat['raw'], y_train=train_dat['label'].values,
            x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

        _, test_pred = model.predict(test_dat['raw'], use_gpu=True)
        mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
            utility.AllMetrics(test_pred, test_dat['label'].values)

    elif args.model == 'gfrnn_ord_reg_no_penalty':
        model = OrdinalRNNClassifier(
            order_penalty=0,
            pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
            pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
        model.build_vocabulary(train_dat['raw'])
        model.initialize_model()
        model.train_model(
            x_train=train_dat['raw'], y_train=train_dat['label'].values,
            x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

        _, test_pred = model.predict(test_dat['raw'], use_gpu=True)
        mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
            utility.AllMetrics(test_pred, test_dat['label'].values)

    elif args.model == 'gfrnn_ord_reg_equalize_class_size':
        model = OrdinalRNNClassifier(
            order_penalty=0, equalize_class_size=True,
            pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
            pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
        model.build_vocabulary(train_dat['raw'])
        model.initialize_model()
        model.train_model(
            x_train=train_dat['raw'], y_train=train_dat['label'].values,
            x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

        _, test_pred = model.predict(test_dat['raw'], use_gpu=True)
        mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
            utility.AllMetrics(test_pred, test_dat['label'].values)

    elif args.model == 'gfrnn_regression':
        model = RNNRegression(
            pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
            pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
        model.build_vocabulary(train_dat['raw'])
        model.initialize_model()
        model.train_model(x_train=train_dat['raw'],
                          y_train=train_dat['label'].values.reshape(-1, 1),
                          x_val=valid_dat['raw'],
                          y_val=valid_dat['label'].values.reshape(-1, 1))

        test_pred = model.predict(test_dat['raw'], use_gpu=True)
        mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
            utility.AllMetrics(test_pred, test_dat['label'].values)

    elif args.model == 'lstm_ord_reg':
        model = OrdinalRNNClassifier(
            order_penalty=1, rnn_type='LSTM',
            pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
            pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
        model.build_vocabulary(train_dat['raw'])
        model.initialize_model()
        model.train_model(
            x_train=train_dat['raw'], y_train=train_dat['label'].values,
            x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

        _, test_pred = model.predict(test_dat['raw'], use_gpu=True)
        mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
            utility.AllMetrics(test_pred, test_dat['label'].values)

    elif args.model == 'logistic_immediate_thresh':
        best_val_mae = 1.0
        best_alpha = None
        for alpha in [0.1, 0.5, 1.0, 5.0, 10.0]:
            model = BaselineClassifier(
                vectorizer_name='tf-idf',
                dimension_reduction_name=None,
                classification_name='ord_logistic_immediate_threshold', seed=0,
                n_gram_low=1, n_gram_high=1,
                min_frequency=2,
                sublinear_tf=True, alpha=alpha)
            model.fit_model_pipeline(train_dat['raw'], train_dat['label'])

            val_pred = model.predict_pipeline(valid_dat['raw'])
            val_mae = utility.MeanAbsError(val_pred, valid_dat['label'].values)
            if val_mae < best_val_mae:
                best_val_mae = val_mae
                best_alpha = alpha
                test_pred = model.predict_pipeline(test_dat['raw'])
                mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
                    utility.AllMetrics(test_pred, test_dat['label'].values)
        print 'best alpha: %5.2f' % best_alpha

    elif args.model == 'logistic_all_thresh':
        best_val_mae = 1.0
        best_alpha = None
        for alpha in [0.1, 0.5, 1.0, 5.0, 10.0]:
            model = BaselineClassifier(
                vectorizer_name='tf-idf',
                dimension_reduction_name=None,
                classification_name='ord_logistic_all_threshold', seed=0,
                n_gram_low=1, n_gram_high=1,
                min_frequency=2,
                sublinear_tf=True, alpha=alpha)
            model.fit_model_pipeline(train_dat['raw'], train_dat['label'])

            val_pred = model.predict_pipeline(valid_dat['raw'])
            val_mae = utility.MeanAbsError(val_pred, valid_dat['label'].values)
            if val_mae < best_val_mae:
                best_val_mae = val_mae
                best_alpha = alpha
                test_pred = model.predict_pipeline(test_dat['raw'])
                mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
                    utility.AllMetrics(test_pred, test_dat['label'].values)
        print 'best alpha: %5.2f' % best_alpha

    elif args.model == 'least_abs_deviation':
        best_val_mae = 1.0
        best_alpha = None
        for alpha in [0.1, 0.5, 1.0, 5.0, 10.0]:
            model = BaselineClassifier(
                vectorizer_name='tf-idf',
                dimension_reduction_name=None,
                classification_name='least_abs_deviation', seed=0,
                n_gram_low=1, n_gram_high=1,
                min_frequency=2,
                sublinear_tf=True, alpha=alpha)
            model.fit_model_pipeline(train_dat['raw'], train_dat['label'])

            val_pred = model.predict_pipeline(valid_dat['raw'])
            val_mae = utility.MeanAbsError(val_pred, valid_dat['label'].values)
            if val_mae < best_val_mae:
                best_val_mae = val_mae
                best_alpha = alpha
                test_pred = model.predict_pipeline(test_dat['raw'])
                mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
                    utility.AllMetrics(test_pred, test_dat['label'].values)
        print 'best alpha: %5.2f' % best_alpha

    elif args.model == 'logistic_regression':
        model = BaselineClassifier(
            vectorizer_name='tf-idf',
            dimension_reduction_name=None,
            classification_name='logistic_regression', seed=0,
            n_gram_low=1, n_gram_high=1,
            min_frequency=2,
            sublinear_tf=True)
        model.fit_model_pipeline(train_dat['raw'], train_dat['label'])

        test_pred = model.predict_pipeline(test_dat['raw'])
        mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
            utility.AllMetrics(test_pred, test_dat['label'].values)

    elif args.model == 'gfrnn_ord_reg_no_batch_norm':
        model = OrdinalRNNClassifier(
            order_penalty=1, batch_normalization=False,
            pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
            pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
        model.build_vocabulary(train_dat['raw'])
        model.initialize_model()
        model.train_model(
            x_train=train_dat['raw'], y_train=train_dat['label'].values,
            x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

        _, test_pred = model.predict(test_dat['raw'], use_gpu=True)
        mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
            utility.AllMetrics(test_pred, test_dat['label'].values)

    elif args.model == 'gfrnn_ord_reg_no_residual_connection':
        model = OrdinalRNNClassifier(
            order_penalty=1, residual_connection=False,
            pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
            pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
        model.build_vocabulary(train_dat['raw'])
        model.initialize_model()
        model.train_model(
            x_train=train_dat['raw'], y_train=train_dat['label'].values,
            x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

        _, test_pred = model.predict(test_dat['raw'], use_gpu=True)
        mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
            utility.AllMetrics(test_pred, test_dat['label'].values)

    elif args.model == 'gfrnn_ord_reg_no_word2vec':
        model = OrdinalRNNClassifier(
            order_penalty=1,
            embedding_vector_length=128)
        model.build_vocabulary(train_dat['raw'])
        model.initialize_model()
        model.train_model(
            x_train=train_dat['raw'], y_train=train_dat['label'].values,
            x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

        _, test_pred = model.predict(test_dat['raw'], use_gpu=True)
        mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i] = \
            utility.AllMetrics(test_pred, test_dat['label'].values)

    else:
        raise ValueError("Model type not recognized!")

    print 'rep: %2d | mae: %0.3f | macro_mae: %0.3f | cs0: %0.3f | cs1: %0.3f | ' \
          'bin_acc: %0.3f | weighted_bin_acc: %0.3f | min: %5.2f' % (
        i, mae[i], macro_mae[i], cs0[i], cs1[i], bin_acc[i], weighted_bin_acc[i],
        (time.time() - start_time) / 60)

    i += 1

# Print summaries
np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
print '\nmae_all:', mae, \
    '\nmean: %5.3f' % mae.mean(), \
    'stderr: %5.3f' % (mae.std() / 10**0.5), \
    '\nmacro_mae_all:', macro_mae, \
    '\nmean: %5.3f' % macro_mae.mean(), \
    'stderr: %5.3f' % (macro_mae.std() / 10**0.5), \
    '\ncs0_all:', cs0, \
    '\nmean: %5.3f' % cs0.mean(), \
    'stderr: %5.3f' % (cs0.std() / 10**0.5), \
    '\ncs1_all:', cs1, \
    '\nmean: %5.3f' % cs1.mean(), \
    'stderr: %5.3f' % (cs1.std() / 10**0.5), \
    '\nbin_acc_all:', bin_acc, \
    '\nmean: %5.3f' % bin_acc.mean(), \
    'stderr: %5.3f' % (bin_acc.std() / 10**0.5), \
    '\nweighted_bin_acc_all:', weighted_bin_acc, \
    '\nmean: %5.3f' % weighted_bin_acc.mean(), \
    'stderr: %5.3f' % (weighted_bin_acc.std() / 10**0.5), '\n'
