# Pytorch
python temp/pytorch-nlp/nlp/word-embeddings/run.py --input_file_name='data/escort_texts.txt' --output_file_name='temp/final_embedding' --method=skip_gram --emb_dimension=128 --batch_size=100 --window_size=5 --iteration=5 --initial_lr=0.025 --min_count=5 --using_hs=False --using_neg=True --num_threads=7 --context_size=5 --seed=0 
# Got nan loss after a while

python temp/pytorch-nlp/nlp/word-embeddings/run.py --input_file_name='data/escort_texts.txt' --output_file_name='temp/final_embedding' --method=cbow --emb_dimension=128 --batch_size=100 --window_size=5 --iteration=3 --initial_lr=0.025 --min_count=5 --using_hs=False --using_neg=True --num_threads=7 --context_size=5 --seed=0 
# < 100 loss after 13h


# Tensorflow
python temp/models/tutorials/embedding/word2vec.py --train_data=data/text8 --eval_data=data/questions-words.txt --save_path=temp/tf_w2v_result/ --statistics_interval=30 --summary_interval=6000 --checkpoint_interval=8 --epochs_to_train=50
# 30.5% eval acc and 146 min in 15 epochs, 35.3% and 247 min in 25 epochs, 39.9% and 495 min in 50 epochs.

# TF, optimized version (note however that the default batch_size and learning rate are different)
python temp/models/tutorials/embedding/word2vec_optimized.py --train_data=data/text8 --eval_data=data/questions-words.txt --save_path=temp/tf_w2v_result/ --statistics_interval=30 --epochs_to_train=150
# 30.4% and 144 min in 55 epochs, 35.1% and 247 min in 94 epochs, 39.2% and 394 min in epoch 150

# TF, optimized version with same hyperparam to check if algorithm implemented is the same
python temp/models/tutorials/embedding/word2vec_optimized.py --train_data=data/text8 --eval_data=data/questions-words.txt --save_path=temp/tf_w2v_result/ --statistics_interval=30 --epochs_to_train=80 --learning_rate=0.2 --num_neg_samples=100 --batch_size=16
# 0% on all. Shouldn't use these hyperparam in word2vec_optimized.

# See how much performance we lose by shrinking embedding size down to 128
python temp/models/tutorials/embedding/word2vec.py --train_data=data/text8 --eval_data=data/questions-words.txt --save_path=temp/tf_w2v_result/ --statistics_interval=30 --summary_interval=6000 --checkpoint_interval=999 --epochs_to_train=70 --embedding_size=128
# 30.8% and 136 min in 15 epochs, 34.8% and 227 min in 25 epochs, 41.5% and 641 in 70 epochs.

# Train embedding with escort data, which includes "tna_ad.pkl" and "backpage_cleaned.pkl". Will be stored as "trained_models/escort_embedding_mini5_200len".
python models/word2vec/word2vec.py --train_data=data/escort_texts.txt --eval_data=data/questions-words.txt --save_path=temp/tf_w2v_result/ --statistics_interval=30 --summary_interval=6000 --checkpoint_interval=999 --epochs_to_train=70
# This embedding reduced test mae to 0.777

# Escort embedding. Reduce embedding vector length from 200 to 128. Will be stored as "trained_models/escort_embedding_mini5_128len".
python models/word2vec/word2vec.py --train_data=data/escort_texts.txt --eval_data=data/questions-words.txt --save_path=temp/tf_w2v_result/ --statistics_interval=30 --summary_interval=6000 --checkpoint_interval=36 --epochs_to_train=70 --embedding_size=128
# Best test mae is now 0.773

# Try minimum frequency of 2. Will be stored as "trained_models/escort_embedding_mini2_128len". 
python models/word2vec/word2vec.py --train_data=data/escort_texts.txt --eval_data=data/questions-words.txt --save_path=temp/tf_w2v_result/ --statistics_interval=30 --summary_interval=6000 --checkpoint_interval=99 --epochs_to_train=70 --embedding_size=128 --min_count=2
# Results are similar to min freq 5

# Re-train escort embedding with new data from Yeng. Now escort_texts has ~168k ads and includes "tna_ad.pkl", "backpage_cleaned.pkl", and "backpage_cleaned2.pkl". Will be stored as "trained_models/escort_embedding_mini5_128len_v2". Results are similar to before.
python models/word2vec/word2vec.py --train_data=data/escort_texts.txt --eval_data=data/questions-words.txt --save_path=temp/tf_w2v_result/ --statistics_interval=30 --summary_interval=6000 --checkpoint_interval=25 --epochs_to_train=70 --embedding_size=128

