from MongoQueue import MongoQueue
from Download import request
from bs4 import BeautifulSoup
import time

spider_queue = MongoQueue('backpage_nc', 'crawl_queue')


def start(url):
    response = request.get(url, 10)
    soup = BeautifulSoup(response.text, 'lxml')
    link_text = [a['href'] for a in soup.find_all('a', href=True) if a.text.strip()]  
    for link in link_text[36:]:
        spider_queue.push(link)

if __name__ == "__main__":
    urls = [
        'http://northcarolina.backpage.com/WomenSeekMen/',
        'http://northcarolina.backpage.com/WomenSeekMen/?page=2',
        'http://northcarolina.backpage.com/WomenSeekMen/?page=3',
        'http://northcarolina.backpage.com/WomenSeekMen/?page=4',
        'http://northcarolina.backpage.com/WomenSeekMen/?page=5'
    ]
    for url in urls:
        time.sleep(5)
        start(url) 
