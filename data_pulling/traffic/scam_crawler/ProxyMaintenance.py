from lxml import etree
from pymongo import MongoClient
from GetHTML import hp
from MongoManagement import mon
from multiprocessing.dummy import Pool as ThreadingPool
import datetime, time, random
import asyncio, aiohttp

class ProxyGet:
    def __init__(self):
        self.urls = {'xici':'http://www.xicidaili.com/wt/page',
                     'kuai':'http://www.kuaidaili.com/free/inha/page/',
                     }

    def get_source_from_html_xici(self, url):
        if url.find('page') > 0:
            _url = url.replace('page', '1')
        else:
            _url = url
        while True:
            try:
                html = hp.get_html(_url).text
                break
            except Exception as e:
                print(e)
        print('URL%s with HTML' % _url)
        if self.parse_links_xici(html):
            this_page = _url.split('/')[-1]
            next_url = _url.replace(this_page, str(int(this_page) + 1))
            self.get_source_from_html_xici(next_url)
        else:
            print('Failure in getting source html')

    def parse_links_xici(self, html):
        xpath_html = etree.HTML(html)
        today_date = str(datetime.date.today())[2:]
        yesterday_date = today_date.replace(today_date[-2:], str(int(today_date[-2:]) - 1))
        
        date_judge = False
        for each_tr_tag in xpath_html.xpath('//*[@id="ip_list"]/tr')[1:]:
            ip_dict = {}
            ip_dict['proxy'] = each_tr_tag.xpath('td[2]/text()')[0] + ':' + each_tr_tag.xpath('td[3]/text()')[0]
            ip_dict['type'] = each_tr_tag.xpath('td[6]/text()')[0]
            ip_dict['time'] = each_tr_tag.xpath('td[10]/text()')[0]

            date_judge = ip_dict['time'].split(' ')[0] != yesterday_date
            if date_judge:
                mon.insert_dict(ip_dict, mop.sourceProxies, 'proxy')
            else:
                return date_judge

        print('SourceProxies')
        return date_judge

    def get_source_from_html_kuai(self, url):
        if url.find('page') > 0:
            _url = url.replace('page', '1')
        else:
            _url = url
        while True:
            html = hp.get_html(_url)
            if html:
                break
        print('Received Kaui%sHTML URL' % _url)
        if self.parse_links_kuai(html.text):
            this_page = _url.split('/')[-2]
            next_url = _url.replace(this_page, str(int(this_page) + 1))
            self.get_source_from_html_kuai(next_url)
        else:
            print('Page not available for Kuai')

    def parse_links_kuai(self, html):
        xpath_html = etree.HTML(html)
        today_time = str(datetime.date.today())
        yesterday_date = today_time.replace(today_time[-2:], str(int(today_time[-2:]) - 1))
        for each_tr in xpath_html.xpath('//*[@id="list"]/table/tbody/tr'):
            ip_dict = {}
            ip_dict['proxy'] = each_tr.xpath('td[1]/text()')[0] + ':' + each_tr.xpath('td[2]/text()')[0]
            ip_dict['type'] = each_tr.xpath('td[4]/text()')[0]
            ip_dict['time'] = each_tr.xpath('td[7]/text()')[0]
            judge_result = yesterday_date == ip_dict['time'].split(' ')[0]
            if judge_result:
                mon.insert_dict(ip_dict, mop.sourceProxies, 'proxy')
            else:
                return judge_result
        return judge_result

    def main(self):
        self.get_source_from_html_xici('http://www.xicidaili.com/nn/page')
        self.get_source_from_html_kuai('http://www.kuaidaili.com/free/inha/page/')

class ProxyCheck:
    def __init__(self):
        self.first_check_url_http = 'http://www.7kk.com'
        self.first_check_url_https = 'https://jinshuju.net/f/YqBTlv'

    def check_http(self, proxy_dict):
        print('Checking%sthe following proxy' % str(proxy_dict['proxy']))


        if proxy_dict['type'].lower() == 'http':
            proxies = {'http': 'http://%s' % proxy_dict['proxy']}
            html = hp.get_html(self.first_check_url_http, proxies=proxies, retry_time=2, timeout=30)
        else:
            proxies = {'https': 'http://%s' % proxy_dict['proxy']}
            html = hp.get_html(self.first_check_url_https, proxies=proxies, retry_time=2, timeout=30)
        if not html:
            return False
        if proxy_dict['type'].lower() == 'http':
            try:
                xpath_html = etree.HTML(html.content.decode('utf-8'))
                title = xpath_html.xpath('/html/head/title/text()')[0]
                if title == '美女图片写真 性感美女图片大全-7kk美女图片':
                    return True
                else:
                    return False
            except Exception as e:
                print(e)
                return False
        elif proxy_dict['type'].lower() == 'https':
            try:
                xpath_html = etree.HTML(html.content)
                title = xpath_html.xpath('/html/head/title/text()')[0]
                if title == '1':
                    return True
                else:
                    return False
            except Exception as e:
                print(e)
                return False

    def check_proxy(self, proxy_dict):
        result = self.check_http(proxy_dict)
        if result:
            if mop.dealProxies.find({'proxy':proxy_dict['proxy']}).count() == 0:
                mon.insert_dict(proxy_dict, mop.dealProxies)  # 存入到dealProxies中
            else:
                print('proxy%s already exists' % proxy_dict['proxy'])
            mon.remove_dict({'proxy': proxy_dict['proxy']}, mop.sourceProxies)
            print('Remove%s' % proxy_dict['proxy'])
        else:
            mon.remove_dict({'proxy': proxy_dict['proxy']}, mop.sourceProxies)
            print('Removed%sfrom source' % proxy_dict['proxy'])

    def main(self):
        proxy_dicts = mop.sourceProxies.find()
        proxy_list = []
        for each_dict in proxy_dicts:
            proxy_list.append(each_dict)
        pool = ThreadingPool(8)
        pool.map(self.check_proxy, proxy_list)
        pool.close()
        pool.join()

class ProxyCheck_:
    def __init__(self):
        self.test_urls = {'http':'http://1212.ip138.com/ic.asp',
                          'https':'https://jinshuju.net/f/YqBTlv'}

    async def manage_proxy(self, proxy_dict, result):
        async with aiohttp.ClientSession() as session:
            try:
                async with session.get(self.test_urls[proxy_dict['type'].lower()], proxy=(proxy_dict['type'].lower() + '://' + proxy_dict['proxy']), timeout=30) as response:
                    result.append({'proxy_dict':proxy_dict, 'status':response.status})
            except:
                result.append({'proxy_dict':proxy_dict, 'status':10060})

    def check_proxy(self, result_dict):
        if result_dict['status'] == 200:
            if mop.dealProxies.find({'proxy':result_dict['proxy_dict']['proxy']}).count() == 0:
                mon.insert_dict(result_dict['proxy_dict'], mop.dealProxies)
            else:
                print('Not able to insert%s' % result_dict['proxy_dict']['proxy'])
            mon.remove_dict({'proxy': result_dict['proxy_dict']['proxy']}, mop.sourceProxies)
            print('Dictionary%sProxy' % result_dict['proxy_dict']['proxy'])
        else:
            mon.remove_dict({'proxy': result_dict['proxy_dict']['proxy']}, mop.sourceProxies)
            print('Not able to insert%s' % result_dict['proxy_dict']['proxy'])

    def main(self):
        proxy_dicts = mop.sourceProxies.find()
        loop = asyncio.get_event_loop()
        result = []
        tasks = [self.manage_proxy(proxy_dict, result) for proxy_dict in proxy_dicts]
        loop.run_until_complete(asyncio.wait(tasks))
        for each_result in result:
            self.check_proxy(each_result)

class ProxyManage_:
    def __init__(self):
        self.test_urls = {'http':'http://1212.ip138.com/ic.asp',
                          'https':'https://jinshuju.net/f/YqBTlv',}
    def count_live_time(self, str_time):
        live_time = ((int(time.strftime('%m', time.localtime())) - int(str_time.split(' ')[0].split('-')[1])) * 30 +
                     (int(time.strftime('%d', time.localtime())) - int(str_time.split(' ')[0].split('-')[2]))) * 24 + \
                    (int(time.strftime('%H', time.localtime())) - int(str_time.split(' ')[1].split(':')[0]))
        return live_time

    def make_new_dict(self, deal_proxy_dict):
        new_dict = {}
        new_dict['type'] = deal_proxy_dict['type']
        new_dict['time'] = deal_proxy_dict['time']
        new_dict['proxy'] = deal_proxy_dict['proxy']
        new_dict['live_time'] = '0'
        new_dict['canVisit'] = 'None'
        new_dict['failed'] = 0
        new_dict['succeed'] = 0
        return new_dict

    def update_data(self, result_dict):
        if result_dict['status'] == 200:
            live_time = self.count_live_time(result_dict['proxy_dict']['time'])
            result_dict['proxy_dict']['live_time'] = str(live_time)
            mop.update_dict({'proxy': result_dict['proxy_dict']['proxy']},
                            {'live_time': result_dict['proxy_dict']['live_time'], 'succeed': result_dict['proxy_dict']['succeed'] + 1},
                            mop.usefulProxies)
            print('%s updated' % result_dict['proxy_dict']['proxy'])
            if result_dict['proxy_dict']['type'].lower() == 'http':
                return 1
            else:
                return 2
        else:
            if result_dict['proxy_dict']['failed'] + 1 > 5:
                if result_dict['proxy_dict']['succeed'] <= 5:
                    mon.remove_dict({'proxy': result_dict['proxy_dict']['proxy']}, mop.usefulProxies)
                    print('Removed%s' % result_dict['proxy_dict']['proxy'])
                else:
                    mop.usefulProxies.update({'proxy':result_dict['proxy_dict']['proxy']}, {'$set':{'succeed':0}})
                if result_dict['proxy_dict']['type'].lower() == 'http':
                    return 3 # https The number of proxy failures, more than 3 times
                else:
                    return 4
            else:
                mop.update_dict({'proxy': result_dict['proxy_dict']['proxy']}, {'failed':result_dict['proxy_dict']['failed'] + 1}, mop.usefulProxies)
                print('Updated%s， failed次数+1' % result_dict['proxy_dict']['proxy'])
                if result_dict['proxy_dict']['type'].lower() == 'http':
                    return 5 # http proxy failed number +1
                else:
                    return 6 # http proxy failed number +1

    async def manage_proxy(self, proxy_dict, result):
        async with aiohttp.ClientSession() as session:
            try:
                async with session.get(self.test_urls[proxy_dict['type'].lower()], proxy=(proxy_dict['type'].lower() + '://' + proxy_dict['proxy']), timeout=30) as response:
                    result.append({'proxy_dict':proxy_dict, 'status':response.status})
            except:
                result.append({'proxy_dict':proxy_dict, 'status':10060})

    def write_note(self, note_result, start_time):
        useful_http, useful_https, failed_http, failed_https, remove_http, remove_https = 0, 0, 0, 0, 0, 0
        for each_return in note_result:
            if each_return == 1:
                useful_http += 1
            elif each_return == 2:
                useful_https += 1
            elif each_return == 3:
                remove_http += 1
            elif each_return == 4:
                remove_https += 1
            elif each_return == 5:
                failed_http += 1
            elif each_return == 6:
                failed_https += 1
        print('Validation completed, time-consuming%s:' % str(time.time() - start_time))
        print('Number of effective agents: Http%s， Https%stotal' % (str(useful_http), str(useful_https)))
        print('Invalid number of agents: Http Total %s， Https%s' % (str(remove_http), str(remove_https)))
        print('Access Failed Number Agent: Http Total%s， Https%s' % (str(failed_http), str(failed_https)))
        with open('proxy_manage.txt', 'a') as fl:
            fl.write('%sProxy Results-------------------------%s' % (str(datetime.date.today()), '\n'))
            fl.write('Effective http%s， https%stotal%s' % (str(useful_http), str(useful_https), '\n'))
            fl.write('Invalid %s, Invalid HTTPs%stotal%s' % (str(remove_http), str(remove_https), '\n'))
            fl.write('Failed：http%stotal， https共%stotal%s' % (str(failed_http), str(failed_https), '\n'))
        fl.close()

    def main(self):
        start_time = time.time()
        print('Starting Proxy Management')
        deal_proxy_dicts = mop.dealProxies.find()
        if deal_proxy_dicts.count() > 0:
            for deal_proxy_dict in deal_proxy_dicts:

                mon.insert_dict(self.make_new_dict(deal_proxy_dict), mop.usefulProxies, 'proxy')
                mon.remove_dict({'proxy':deal_proxy_dict['proxy']}, mop.dealProxies)
        useful_proxy_dicts = mop.usefulProxies.find(no_cursor_timeout=True)
        result = []
        tasks = [self.manage_proxy(proxy, result) for proxy in useful_proxy_dicts]
        loop2 = asyncio.get_event_loop()
        loop2.run_until_complete(asyncio.wait(tasks))
        loop2.close()
        note_result = []
        for each_result in result:
            note_result.append(self.update_data(each_result))
        self.write_note(note_result, start_time)

class MongoPro:
    def __init__(self):
        self.client = MongoClient('localhost', 27017)
        self.db = self.client.Proxy
        self.sourceProxies = self.db.SourceProxy
        self.dealProxies = self.db.DealProxy
        self.usefulProxies = self.db.UsefulProxy

    def update_dict(self, verify_dict, update_dict, target_collection):
        target_collection.update(verify_dict, {'$set':update_dict})
        print('updated target collection')

class ProxyUse:
    def get_random_proxy(self, type):
        proxy_dicts = mop.usefulProxies.find({'type':type.upper()})
        proxy_dict = proxy_dicts[random.randint(0, proxy_dicts.count())]
        return {type:'http://%s' % proxy_dict['proxy']}

    def get_all_proxy(self, type):
        proxy_dicts = mop.usefulProxies.find({'type':type.upper()})
        result = []
        for proxy_dict in proxy_dicts:
            result.append({'http':'http://%s' % proxy_dict['proxy']})
        return result

    def update_used_proxy(self, proxy, result):
        try:
            proxy_dict = mop.usefulProxies.find({'proxy': proxy})[0]
        except IndexError:
            print('Index Error, none found :(')
            return
        if result == 'success':
            live_time = prom.count_live_time(proxy_dict['time'])
            proxy_dict['live_time'] = str(live_time)
            mop.update_dict({'proxy': proxy}, {'live_time': proxy_dict['live_time']}, mop.usefulProxies)
        else:
            failed_count = proxy_dict['failed'] + 1
            if failed_count > 10:
                mop.usefulProxies.remove({'proxy':proxy})
            else:
                mop.usefulProxies.update({'proxy': proxy},{'$set':{'failed':failed_count}})

    def get_proxy_queue(self, type):
        proxy_dicts = mop.usefulProxies.find({'type': type.upper()})

if __name__ == '__main__':
    mop = MongoPro()
    prog = ProxyGet()
    proc_ = ProxyCheck_()  
    prom_ = ProxyManage_()
    prou = ProxyUse()
    prog.main()
    proc_.main()
    prom_.main()
else:
    mop = MongoPro()
    prog = ProxyGet()
    proc = ProxyCheck_()
    prom = ProxyManage_()
    prou = ProxyUse()