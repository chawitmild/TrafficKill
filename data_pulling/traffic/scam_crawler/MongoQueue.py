# !usr/bin/python
# -*-coding:utf-8-*-

from pymongo import MongoClient, errors
from datetime import datetime, timedelta


class MongoQueue():

    OUTSTANDING = 1
    PROCESSING = 2
    COMPLETE = 3

    def __init__(self, db, collection, timeout = 3):
        self.client = MongoClient()
        self.client_db = self.client[db]
        self.db = self.client_db[collection]
        self.timeout = timeout

    def __bool__(self):
        record = self.db.find_one(
            {'status': {'$ne': self.COMPLETE}}
        )
        return True if record else False

    def push(self, url):
        try:
            self.db.insert({'_id': url, 'status': self.OUTSTANDING })
            print('Success!')
        except errors.DuplicateKeyError as e:
            print(url, 'Skipping this duplicate')
            pass

    def push_post(self, url, mapping):
        try:
            self.db.insert({'_id':url, 'data': mapping})
            print('Successful insertion of mapping!')
        except errors.DuplicateKeyError as e:
            print(e)
            pass                    

    def pop(self):
        record = self.db.find_and_modify(
            query={'status': self.OUTSTANDING},
            update={'$set': {'status': self.PROCESSING, 'timestamp': datetime.now()}}
        )
        if record:
            return record['_id']
        else:
            self.repair()
            raise KeyError

    def pop_url(self, url):
        record = self.db.find_one({'_id': url})
        return record['_id']

    def complete(self, url):
        self.db.update({'_id': url}, {'$set': {'status': self.COMPLETE}})

    def repair(self):
        record = self.db.find_and_modify(
            query={
                'timestamp': {'$lt': datetime.now() - timedelta(seconds=self.timeout)},
                'status': {'$ne': self.COMPLETE}
            },
            update={
                '$set': {'status': self.COMPLETE}
            }
        )

        if record:
            print('URL', record['_id'])

    def clear(self):
        self.db.drop()