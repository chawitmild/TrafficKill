import emoji
import string
import re
import time
import numpy as np
import pandas as pd
from tna_data_prep import DataClean

# In the original dataset, each comma in the text created a column break.
# To resolve this problem, we pre-specify a large list of columns to read the
# variable columns, replace NaN's with empty strings, then concatenate the columns to
# make all the text in one column, and finally separate the title from the body with
# the '>     ' token.
max_col = 50
col_names = range(max_col)
data = pd.read_csv('data/marinus_analytics_trafficking_10K.csv',
                   header=None, names=col_names, low_memory=False)
data = data.fillna('')
# Note that column0 contains id, column1 contains label. Then supposedly column2
# contains title and column3 contains body, but they are both broken up into variable
# columns. We join all of them.
for i in range(3, max_col):
    data[2] = data[2] + ' ' + data[i]
data[2] = data[2].str.strip()
data = data.iloc[:, :3]
# Seperate title from body. After some scrutiny, I find that All bodies begin with
# '>     ' except for those with titles missing.
data['title'] = ''
data['raw'] = ''
for i in range(data.shape[0]):
    if '>     ' in data.iloc[i, 2]:
        split_str = data.iloc[i, 2].split('>     ')
        data.loc[i, 'title'], data.loc[i, 'raw'] = \
            split_str[0].strip(), split_str[1].strip()
    else:
        # In this case fill the body and leave the title as empty string
        data.loc[i, 'raw'] = data.iloc[i, 2].strip()

# Only keep relevant columns after the preliminary cleaning
data.rename(columns={0: 'id', 1: 'label'}, inplace=True)
data = data[['id', 'label', 'title', 'raw']]
data.to_csv('data/trafficking10k.csv', index=None)
# data = pd.read_csv('Trafficking_10k_data/trafficking10k.csv', na_filter=False)

# Use the same function from tna_data_prep to clean the data. Refer to
# tna_data_prep.py for details. DataClean() assumes that the 'title' column contains
# the title and 'raw' contains the body of ad.
data = DataClean(data, strip_tab=True, strip_line_break=True, strip_punctuation=True,
                 convert_lower_case=True, find_number=False, remove_number=True,
                 unicode_text=False)
data.to_pickle("data/trafficking10k_cleaned.pkl")