import urllib
from urllib.error import HTTPError
from urllib.request import urlretrieve
#import zipfile #In case we'd like to save these as a zip file


#Test url:
#url = "https://eyeonhousing.files.wordpress.com/2012/11/blog-gdp-2012_10_1.jpg"

imageList = [] #Put list of images to be downloaded here
local_path = "/Users/Conor/Desktop/img_" #Put path name to download destination here


#Make counters for number of errors that occur, and number of iterations
errors = 0
i = 0

#Loop through every image in the list of images, pull them from online, then 
#add them to some local directory:

for image_url in imageList:
    try:
        urlretrieve(image_url, local_path + str(i) + ".jpg")
        i += i
    except ValueError as err:
         errors += errors
         print(err)
         pass
    except FileNotFoundError as err:
         errors += errors
         print(err)   # something wrong with local path
         pass
    except HTTPError as err:
         errors += errors
         print(err)  # something wrong with url
         pass
    
    
    
    

         
         
         