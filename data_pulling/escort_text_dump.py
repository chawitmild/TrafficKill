"""
Dump all the texts to a txt file from datasets prepared by tna_data_prep.py. In the
produced txt file, each line corresponds to a post. This file will be used as the
input file for the word2vec model.
"""
import csv
import pandas as pd

# Each tna dataset has a 'raw' column that contains the content of a post, and a
# 'title' column that contains the title. One can decide whether to include the title
# in the training data.
include_title = True
# Different post types have different styles, need to experiment whether the model
# works better by including all or only some of them.
post_type_list = ['ad']
# post_type_list = ["ad", "review", "thread"]

for post_type in post_type_list:
    # Assume that the datasets produced by tna_data_prep.py are in the same directory
    data = pd.read_pickle("data/tna_" + post_type + ".pkl")
    if include_title:
        data['raw'] = data['title'] + ' ' + data['raw']
    # Remove empty rows
    data = data.loc[data['raw'] != '', :]
    # mode='a' enables appending to the save file
    data.raw.to_frame().to_csv(
        r'data/escort_texts.txt', header=None, index=None, sep=' ', mode='a',
        encoding='utf-8', quoting=csv.QUOTE_NONE, escapechar=' ')
    print "Finished dumping %s" % post_type

# Also dump backpage data
data = pd.read_pickle('data/backpage_cleaned.pkl')
if include_title:
    data['raw'] = data['title'] + ' ' + data['raw']
data = data.loc[data['raw'] != '', :]
data.raw.to_frame().to_csv(
    r'data/escort_texts.txt', header=None, index=None, sep=' ', mode='a',
    encoding='utf-8', quoting=csv.QUOTE_NONE, escapechar=' ')